
### Install from repository
Download & unpack the files, navigate to the directory and run:
```
composer install
```
After it has completed, run:
```
npm install
```
Copy the example .env file:
```
cp .env.example .env
```
Generate an application key:
```
php artisan key:generate
```
Run Mix tasks:
```
npm run dev
```
View the website:
```
php artisan serve
```
GitHub: https://github.com/tomtomdu73/Laravel-5.5-CoreUI-ReactJS
