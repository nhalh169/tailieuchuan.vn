<?php
/**
 *  HomeController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 23/05/2018
 * Modified at: 23/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }
}