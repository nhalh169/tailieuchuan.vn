<?php
/**
 *  CategoryController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Model\CategoriesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{

    public function addCategory(Request $request)
    {
        $data = $request->all();
        try {
            $groupUser = new CategoriesModel();
            $groupUser->fill($data);
            $groupUser->save();
            return $this->responseSuccess($groupUser);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editCategory($id,Request $request)
    {
        $data = $request->all();
        try {
            $groupUser = CategoriesModel::find($id);
            if($groupUser) {
                $groupUser->fill($data);
                $groupUser->save();
                return $this->responseSuccess($groupUser);
            } else {
                return $this->responseError(-1, 'Can not find this user');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function deleteCategory($id)
    {
        try {
            $groupUser = CategoriesModel::find($id);
            $groupUser->delete();
            return $this->responseSuccess($groupUser);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllCategory(Request $request)
    {
        try {
            $order = $request->get('order');
            if(isset($order)) {
                $listCategorys =  CategoriesModel::orderBy($order)->get();
            } else {
                $listCategorys =  CategoriesModel::orderBy('id','ASC')->get();
            }
            return $this->responseSuccess($listCategorys);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getAllData()
    {
        try {
            $listCategorys =  CategoriesModel::getAllData();
            return $this->responseSuccess($listCategorys);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getDetailCategory($id)
    {
        try {
            return $this->responseSuccess(CategoriesModel::find($id));
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

}