<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Response;
use Request;
if (empty($_SESSION)  && !isset($_SESSION))  {
    session_start();
}

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){
//        $this->resetSesstion();
    }

    private function resetSesstion(){
        $time = $_SERVER['REQUEST_TIME'];
        $timeout_duration = 1800;
        if (isset($_SESSION['LAST_ACTIVITY']) &&
            ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
            session_unset();
            session_destroy();
            session_start();
        }
        $_SESSION['LAST_ACTIVITY'] = $time;
    }

    public function responseSuccess( $data = null ){

        //Protect APIs, donnot show results when calling from URL browwser address
//        if( !Request::wantsJson()){
//            return response('This is private API!!!');
//        }
        return Response::json([
            "success" => true,
            "result"  => $data
        ] );
    }

    public function responseError( $errorCode, $msg=null ){
        return Response::json([
            "success"       => false,
            "error_code"    => $errorCode,
            "message"       => $msg
        ] );
    }
}
