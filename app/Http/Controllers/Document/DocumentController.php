<?php
/**
 *  DocumentController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\Document;

use App\Http\Controllers\Controller;
use App\Model\DocumentsModel;
use App\Model\DocumentSubjectModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DocumentController extends Controller
{
    public function addDocument(Request $request)
    {
        $data = $request->all();
        try {
            $document = new DocumentsModel();
            $document->fill($data);
            $document->save();
            $this->updateDocumentSubject($document->id, [], $data[subject_ids]);
            return $this->responseSuccess($document);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editDocument($id,Request $request)
    {
        $data = $request->all();
        try {
            $document = DocumentsModel::find($id);
            if($document) {
                $document->fill($data);
                $document->save();
				$detailDoc = DocumentsModel::getDetailInfo($id);
				$this->updateDocumentSubject($id, $detailDoc[0]->subject_ids, $data['subject_ids']);
                return $this->responseSuccess($document);
            } else {
                return $this->responseError(-1, 'Can not find this subject');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

	private function updateDocumentSubject($documentId = 0, $oldSubjects = '', $newSubjects = '')
	{
		if($documentId == 0) {
			return;
		}
		$listOldSubject = explode(',', $oldSubjects);
		$listNewSubject = explode(',', $newSubjects);
		if($oldSubjects == '') {
			$listOldSubject = [];
		}
		if($newSubjects == '') {
			$listNewSubject = [];
		}
		$listRemoveSubject = [];
		$listAddSubject = [];
		foreach ($listOldSubject as $oldSubject) {
			$removeSubject = true;
			foreach ($listNewSubject as $newSubject) {
				if($newSubject == $oldSubject) {
					$removeSubject = false;
				}
			}
			if($removeSubject) {
				array_push($listRemoveSubject, $oldSubject);
			}
		}
		foreach ($listNewSubject as $newSubject) {
			$addSubject = true;
			foreach ($listOldSubject as $oldSubject) {
				if($newSubject == $oldSubject) {
					$addSubject = false;
				}
			}
			if($addSubject) {
				array_push($listAddSubject, $newSubject);
			}
		}
		DocumentSubjectModel::where('document_id', $documentId)->whereIn('subject_id', $listRemoveSubject)->delete();
		foreach ($listAddSubject as $newSubject) {
			$newMapping = new DocumentSubjectModel();
			$newMapping->document_id = $documentId;
			$newMapping->subject_id = $newSubject;
			$newMapping->save();
		}
		return;
	}

    public function deleteDocument($id)
    {
        try {
            $document = DocumentsModel::find($id);
            $document->delete();
            return $this->responseSuccess($document);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllDocument(Request $request)
    {
        try {
//            $order = $request->get('order');
//            if(isset($order)) {
//                $listDocuments =  DocumentsModel::orderBy($order)->get();
//            } else {
//                $listDocuments =  DocumentsModel::orderBy('id','ASC')->get();
//            }
			$listDocument =  DocumentsModel::all();
            return $this->responseSuccess($listDocument);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function getDetailDocument($id)
	{
		try {
			$document = DocumentsModel::find($id);
//			if($document) {
//				return $this->responseSuccess($document[0]);
//			}
			return $this->responseSuccess($document[0]);
//			return $this->responseError(-1, 'This id is not exist!');
		} catch(\Exception $e) {
			return $this->responseError(-1, $e->getMessage());
		}
	}

    public function getListApproved()
    {
        try {
            $listDocuments =  DocumentsModel::getListApproved();
            return $this->responseSuccess($listDocuments);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getListUnapproved()
    {
        try {
            $listDocuments =  DocumentsModel::getListUnapproved();
            return $this->responseSuccess($listDocuments);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

}