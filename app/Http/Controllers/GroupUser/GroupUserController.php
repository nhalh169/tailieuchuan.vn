<?php
/**
 *  GroupUserController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\GroupUser;

use App\Http\Controllers\Controller;
use App\Model\GroupUsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GroupUserController extends Controller
{

    public function addGroupUser(Request $request)
    {
        $data = $request->all();
        try {
            $groupUser = new GroupUsersModel();
            $groupUser->fill($data);
            $groupUser->save();
            return $this->responseSuccess($groupUser);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editGroupUser($id,Request $request)
    {
        $data = $request->all();
        try {
            $groupUser = GroupUsersModel::find($id);
            if($groupUser) {
                $groupUser->fill($data);
                $groupUser->save();
                return $this->responseSuccess($groupUser);
            } else {
                return $this->responseError(-1, 'Can not find this user');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function deleteGroupUser($id)
    {
        try {
            $groupUser = GroupUsersModel::find($id);
            $groupUser->delete();
            return $this->responseSuccess($groupUser);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllGroupUser(Request $request)
    {
        try {
            $order = $request->get('order');
            if(isset($order)) {
                $listGroupUsers =  GroupUsersModel::orderBy($order)->get();
            } else {
                $listGroupUsers =  GroupUsersModel::orderBy('id','ASC')->get();
            }
            return $this->responseSuccess($listGroupUsers);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getAllData()
    {
        try {
            $listGroupUsers =  GroupUsersModel::getAllData();
            return $this->responseSuccess($listGroupUsers);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getDetailGroupUser($id)
    {
        try {
            return $this->responseSuccess(GroupUsersModel::find($id));
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

}