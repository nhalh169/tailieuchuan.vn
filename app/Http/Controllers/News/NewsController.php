<?php
/**
 *  NewsController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use App\Model\MappingNewsCategoryModel;
use App\Model\NewsesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NewsController extends Controller
{
    public function addNews(Request $request)
    {
        $data = $request->all();
        try {
            $news = new NewsesModel();
            $news->fill($data);
            $news->save();
			$this->updateMappingNewsCategory($news->id, '', $data['categories']);
            if(isset($data['avatar_data'])) {
				$this->saveNewsAvatar($data['avatar_data'], $news->id);
			}
            return $this->responseSuccess($news);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    private function saveNewsAvatar($pic, $id)
	{
		list(, $pic) = explode(';', $pic);
		list(, $pic)      = explode(',', $pic);
		$pic = base64_decode($pic);
		file_put_contents("img/news-avatars/news-$id.png" , $pic);
		return;
	}

    public function editNews($id,Request $request)
    {
        $data = $request->all();
        try {
            $news = NewsesModel::find($id);
            if($news) {
            	$detailNews = NewsesModel::getDetailNews($id);
				$this->updateMappingNewsCategory($id, $detailNews[0]->categories, $data['categories']);
                $news->fill($data);
                $news->save();
				if(isset($data['avatar_data'])) {
					$this->saveNewsAvatar($data['avatar_data'], $news->id);
				}
                return $this->responseSuccess(NewsesModel::getDetailNews($id)[0]);
            } else {
                return $this->responseError(-1, 'Can not find this user');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    private function updateMappingNewsCategory($newsId = 0, $oldCategories = '', $newCategories = '')
	{
    	if($newsId == 0) {
    		return;
		}
		$listOldCategory = explode(',', $oldCategories);
		$listNewCategory = explode(',', $newCategories);
		if($oldCategories == '') {
			$listOldCategory = [];
		}
		if($newCategories == '') {
			$listNewCategory = [];
		}
		$listRemoveCategory = [];
		$listAddCategory = [];
		foreach ($listOldCategory as $oldCategory) {
			$removeCategory = true;
			foreach ($listNewCategory as $newCategory) {
				if($newCategory == $oldCategory) {
					$removeCategory = false;
				}
			}
			if($removeCategory) {
				array_push($listRemoveCategory, $oldCategory);
			}
		}
		foreach ($listNewCategory as $newCategory) {
			$addCategory = true;
			foreach ($listOldCategory as $oldCategory) {
				if($newCategory == $oldCategory) {
					$addCategory = false;
				}
			}
			if($addCategory) {
				array_push($listAddCategory, $newCategory);
			}
		}
		MappingNewsCategoryModel::whereIn('news_category_id', $listRemoveCategory)->delete();
		foreach ($listAddCategory as $newCategory) {
			$newMapping = new MappingNewsCategoryModel();
			$newMapping->news_id = $newsId;
			$newMapping->news_category_id = $newCategory;
			$newMapping->save();
		}
		return;
	}

    public function deleteNews($id)
    {
        try {
            $news = NewsesModel::find($id);
            $news->delete();
            return $this->responseSuccess($news);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllNews(Request $request)
    {
        try {
//            $order = $request->get('order');
//            if(isset($order)) {
//                $listNewss =  NewsesModel::orderBy($order)->get();
//            } else {
//                $listNewss =  NewsesModel::orderBy('id','ASC')->get();
//            }
			$listNews =  NewsesModel::getAllData();
            return $this->responseSuccess($listNews);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function getDetailNews($id)
	{
		try {
			$news = NewsesModel::getDetailNews($id);
			if($news) {
				return $this->responseSuccess($news[0]);
			}
			return $this->responseError(-1, 'This id is not exist!');
		} catch(\Exception $e) {
			return $this->responseError(-1, $e->getMessage());
		}
	}

    public function getAllData()
    {
        try {
            $listNewss =  NewsesModel::getAllData();
            return $this->responseSuccess($listNewss);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

}