<?php
/**
 *  NewsCategoryController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\NewsCategory;

use App\Http\Controllers\Controller;
use App\Model\NewsCategoriesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NewsCategoryController extends Controller
{

    public function addNewsCategory(Request $request)
    {
        $data = $request->all();
        try {
            $newsCategory = new NewsCategoriesModel();
            $newsCategory->fill($data);
            $newsCategory->save();
            return $this->responseSuccess($newsCategory);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editNewsCategory($id,Request $request)
    {
        $data = $request->all();
        try {
            $newsCategory = NewsCategoriesModel::find($id);
            if($newsCategory) {
                $newsCategory->fill($data);
                $newsCategory->save();
                return $this->responseSuccess($newsCategory);
            } else {
                return $this->responseError(-1, 'Can not find this user');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function deleteNewsCategory($id)
    {
        try {
            $newsCategory = NewsCategoriesModel::find($id);
            $newsCategory->delete();
            return $this->responseSuccess($newsCategory);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllNewsCategory(Request $request)
    {
        try {
            $order = $request->get('order');
            if(isset($order)) {
                $listNewsCategorys =  NewsCategoriesModel::orderBy($order)->get();
            } else {
                $listNewsCategorys =  NewsCategoriesModel::orderBy('id','ASC')->get();
            }
            return $this->responseSuccess($listNewsCategorys);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getAllData()
    {
        try {
            $listNewsCategorys =  NewsCategoriesModel::getAllData();
            return $this->responseSuccess($listNewsCategorys);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getDetailNewsCategory($id)
    {
        try {
            return $this->responseSuccess(NewsCategoriesModel::find($id));
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

}