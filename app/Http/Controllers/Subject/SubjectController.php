<?php
/**
 *  SubjectController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\Subject;

use App\Http\Controllers\Controller;
use App\Model\SubjectsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubjectController extends Controller
{
    public function addSubject(Request $request)
    {
        $data = $request->all();
        try {
            $news = new SubjectsModel();
            $news->fill($data);
            $news->save();
            return $this->responseSuccess($news);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editSubject($id,Request $request)
    {
        $data = $request->all();
        try {
            $news = SubjectsModel::find($id);
            if($news) {
                $news->fill($data);
                $news->save();
                return $this->responseSuccess($news);
            } else {
                return $this->responseError(-1, 'Can not find this subject');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function deleteSubject($id)
    {
        try {
            $news = SubjectsModel::find($id);
            $news->delete();
            return $this->responseSuccess($news);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllSubject(Request $request)
    {
        try {
//            $order = $request->get('order');
//            if(isset($order)) {
//                $listSubjects =  SubjectsModel::orderBy($order)->get();
//            } else {
//                $listSubjects =  SubjectsModel::orderBy('id','ASC')->get();
//            }
			$listSubject =  SubjectsModel::all();
            return $this->responseSuccess($listSubject);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function getDetailSubject($id)
	{
		try {
			$news = SubjectsModel::find($id);
//			if($news) {
//				return $this->responseSuccess($news[0]);
//			}
			return $this->responseSuccess($news[0]);
//			return $this->responseError(-1, 'This id is not exist!');
		} catch(\Exception $e) {
			return $this->responseError(-1, $e->getMessage());
		}
	}

    public function getAllData()
    {
        try {
            $listSubjects =  SubjectsModel::all();
            return $this->responseSuccess($listSubjects);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

}