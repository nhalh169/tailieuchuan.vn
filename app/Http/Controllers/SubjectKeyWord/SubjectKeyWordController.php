<?php
/**
 *  SubjectKeyWordController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 01/12/2018
 * Modified at: 04/12/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\SubjectKeyWord;

use App\Http\Controllers\Controller;
use App\Model\SubjectKeyWordsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubjectKeyWordController extends Controller
{
    public function addSubjectKeyWord(Request $request)
    {
        $data = $request->all();
        try {
            $keyWord = new SubjectKeyWordsModel();
            $keyWord->fill($data);
            $keyWord->save();
            return $this->responseSuccess($keyWord);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function editSubjectKeyWord($id,Request $request)
    {
        $data = $request->all();
        try {
            $keyWord = SubjectKeyWordsModel::find($id);
            if($keyWord) {
                $keyWord->fill($data);
                $keyWord->save();
                return $this->responseSuccess($keyWord);
            } else {
                return $this->responseError(-1, 'Can not find this subjectKeyWord');
            }
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function deleteSubjectKeyWord($id)
    {
        try {
            $keyWord = SubjectKeyWordsModel::find($id);
            $keyWord->delete();
            return $this->responseSuccess($keyWord);
        } catch(\Exception $e) {
            $this->responseSuccess(-1, $e->getMessage());
        }
    }

    public function getAllSubjectKeyWord(Request $request)
    {
        try {
//            $order = $request->get('order');
//            if(isset($order)) {
//                $listSubjectKeyWords =  SubjectKeyWordsModel::orderBy($order)->get();
//            } else {
//                $listSubjectKeyWords =  SubjectKeyWordsModel::orderBy('id','ASC')->get();
//            }
			$listSubjectKeyWord =  SubjectKeyWordsModel::getAllData();
            return $this->responseSuccess($listSubjectKeyWord);
        } catch(\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function getDetailSubjectKeyWord($id)
	{
		try {
			$keyWord = SubjectKeyWordsModel::find($id);
//			if($keyWord) {
//				return $this->responseSuccess($keyWord[0]);
//			}
			return $this->responseSuccess($keyWord[0]);
//			return $this->responseError(-1, 'This id is not exist!');
		} catch(\Exception $e) {
			return $this->responseError(-1, $e->getMessage());
		}
	}

    public function getAllData()
    {
        try {
            $listSubjectKeyWords =  SubjectKeyWordsModel::all();
            return $this->responseSuccess($listSubjectKeyWords);
        } catch(\Exception $e) {
            return $this->responseError(-1,$e->getMessage());
        }
    }

}