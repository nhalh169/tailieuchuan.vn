<?php
/**
 *  UserController.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
class UserController extends Controller
{

    public function login(Request $request)
    {
        $userData = $request->all();
        try {
            $users = UsersModel::where('email', $userData['email'])->get();
            if(count($users) == 0) {
                $user = new UsersModel();
                $user->name = $userData['name'];
                $user->email = $userData['email'];
                $user->img_url = $userData['imageUrl'];
                $user->role_id = 0;
                $user->save();
                $this->sendCreateUserEmail($user);
            } else {
                $user = $users[0];
                if ($user->img_url != $userData['imageUrl']) {
                    $user->img_url = $userData['imageUrl'];
                    $user->save();
                }
            }
            //save log login
            $log = new LogUserLoginModel();
            $log->user_id = $user->id;
            $log->user_email = $user->email;
            $log->save();

            if($user->role_id == 0 || $user->role_id == "") {
                return $this->responseError(0,$user->id);
            }
            $role = RolesModel::find($user->role_id);
            // save session
            $_SESSION["user_id"] = $user->id;
            $_SESSION["user_name"] = $user->name;
            $_SESSION["user_email"] = $user->email;
            $_SESSION["user_img_url"] = $user->img_url;
            $_SESSION["user_role_code"] = $role->code;
            $_SESSION["user_product_ids"] = $user->getProductIds();

            return $this->responseSuccess($user);
        } catch(\Exciption $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

    public function logout()
    {
        $_SESSION["user_id"] = "";
        $_SESSION["user_name"] = "";
        $_SESSION["user_email"] = "";
        $_SESSION["user_img_url"] = "";
        $_SESSION["user_role_id"] = "";
        $_SESSION["user_product_ids"] = "";

        return $this->responseSuccess("");
    }

    public function addUser(Request $request)
    {
        $data = $request->all();
        try{
            $user = new UsersModel();
            $user->fill($data);
            $user->save();
            return $this->responseSuccess($user);
        }catch (\Exception $e){
            Log::debug($e->getMessage());
            return $this->responseSuccess(-1,$e->getMessage());
        }
    }

    public function editUser($id, Request $request)
    {
        $data = $request->all();
        try {
            $user = UsersModel::find($id);
            if ($user) {
                $user->fill($data);
                $user->save();
                return $this->responseSuccess($user);
            } else {
                return $this->responseError(-1, 'Can not find this user');
            }
        } catch (\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

//    public function deleteUser($id){
//        try{
//            $user = UsersModel::find($id);
////            $reportTemplates = DashboardModel::where('user_id','=',$id)->get();
////            foreach ($reportTemplates as $reportTemplate){
////                $reportTemplate->deleteAllInfo();
////            }
//            $user->delete();
//            return $this->responseSuccess($user);
//        }catch (\Exception $e){
//            Log::debug($e->getMessage());
//            $this->responseSuccess(-1,$e->getMessage());
//        }
//    }

    public function getAllUser(Request $request){
        try{
            $order = $request->get('order');
            if(isset($order)){
                $listUsers =  UsersModel::orderBy($order)->get();
            } else {
                $listUsers =  UsersModel::orderBy('id','DESC')->get();
            }
//            foreach ($listUsers as $user){
//                $user->product_ids = $user->getProductIds();
//            }
            return $this->responseSuccess($listUsers);
        }catch (\Exception $e){
            Log::debug($e->getMessage());
            return $this->responseError(-1,$e->getMessage());
        }
    }

    public function getDetailUser($id){
        try {
            return $this->responseSuccess(UsersModel::find($id));
        } catch (\Exception $e) {
            return $this->responseError(-1, $e->getMessage());
        }
    }

}