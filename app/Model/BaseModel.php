<?php
/**
 *  BaseModel.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    const CONNECTION = 'base_connection';

    public function __construct()
    {
        /**
         * Select database connection
         * - Pls check detail configuration in the config/database.php
         */
        $this->setConnection( self::CONNECTION );
    }


    /**
     * Get table name
     */
    public static function table()
    {
        return with(new static)->getTable();
    }
}

?>