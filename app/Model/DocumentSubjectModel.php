<?php
/**
 *  Newses.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class DocumentSubjectModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document_subject';
    protected $fillable = [
        'document_id',
        'subject_id',
    ];

}