<?php
/**
 *  Newses.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class DocumentsModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documents';
    protected $fillable = [
        'name',
        'category_id',
        'link_file',
        'doc_type',
        'size',
        'price',
        'page_count',
        'description',
        'creator_id',
        'approver_id',
        'key_word',
        'hightlight_in_category',
        'hightlight_in_subject',
    ];

    public static function getDetailInfo($id = 0)
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
                  d.*,
                  GROUP_CONCAT(ds.subject_id) as subject_ids
                FROM
                    documents d
                LEFT JOIN `document_subject` ds ON ds.document_id = d.id
                WHERE d.id = $id
            ");
	}

	public static function getListApproved()
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
					d.*,
					GROUP_CONCAT(ds.subject_id) as subject_ids
                FROM
                    documents d
                LEFT JOIN `document_subject` ds ON ds.document_id = d.id
                WHERE d.approver_id IS NOT NULL 
                AND d.approver_ID <> 0
				GROUP BY d.id
            ");
	}

	public static function getListUnapproved()
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
					d.*,
					GROUP_CONCAT(ds.subject_id) as subject_ids
                FROM
                    documents d
                LEFT JOIN `document_subject` ds ON ds.document_id = d.id
                WHERE d.approver_id IS NULL 
                OR d.approver_ID = 0
				GROUP BY d.id
            ");
	}

}