<?php
/**
 *  GroupUsersModel.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class GroupUsersModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group_users';
    protected $fillable = [
        'name',
    ];

    public static function getAllData()
    {
        return DB::connection( self::CONNECTION )
            ->select("
                SELECT
                  g.*, count(u.id) AS number_user
                FROM
                    group_users g
                LEFT JOIN `users` u ON u.group_id = g.id
                GROUP BY g.id
            ");
    }

}