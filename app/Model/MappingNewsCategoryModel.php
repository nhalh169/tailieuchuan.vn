<?php
/**
 *  Newses.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class MappingNewsCategoryModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mapping_news_category';
    protected $fillable = [
        'news_id',
        'news_category_id',
    ];

}