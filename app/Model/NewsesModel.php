<?php
/**
 *  Newses.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class NewsesModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'newses';
    protected $fillable = [
        'name',
        'avatar',
        'description',
        'content',
        'key_word',
        'creator',
        'approver',
        'order',
        'hightlight_on_ homepage',
        'hightlight_on_ category',
    ];

    public static function getDetailNews($id = 0)
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
                  n.*,
                  GROUP_CONCAT(m.news_category_id) as categories
                FROM
                    newses n
                LEFT JOIN `mapping_news_category` m ON m.news_id = n.id
                WHERE n.id = $id
            ");
	}

	public static function getAllData()
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
					n.*,
					GROUP_CONCAT(m.news_category_id) as categories
                FROM
                    newses n
                LEFT JOIN `mapping_news_category` m ON m.news_id = n.id
				GROUP BY n.id
            ");
	}

}