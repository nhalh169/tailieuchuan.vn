<?php
/**
 *  Newses.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class SubjectKeyWordsModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subject_key_words';
    protected $fillable = [
        'name',
        'subject_id',
    ];

    public static function getDetailNews($id = 0)
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
                  n.*,
                  GROUP_CONCAT(m.news_category_id) as categories
                FROM
                    newses n
                LEFT JOIN `mapping_news_category` m ON m.news_id = n.id
                WHERE n.id = $id
            ");
	}

	public static function getAllData()
	{
		return DB::connection( self::CONNECTION )
			->select("
                SELECT
					k.*, c.id AS category_id
				FROM
					subject_key_words k
				LEFT JOIN `subjects` s ON s.id = k.subject_id
				LEFT JOIN categories c ON c.id = s.category_id
            ");
	}

}