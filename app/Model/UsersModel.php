<?php
/**
 *  UsersModel.php
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


namespace App\Model;
use Illuminate\Support\Facades\DB;

class UsersModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $fillable = [
        'full_name',
        'phone',
        'birth_day',
        'title_id',
        'class',
        'pay_number',
        'pay_name',
        'group_id',
        'discount',
        'fixed_discount',
        'prioritize_member',
        'block_status',
        'block_message',
    ];

}