<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\UserModel;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection( BaseModel::CONNECTION )
            ->create(UserModel::table(), function (Blueprint $table) {
                $table->increments('id');
                $table->string('email', 256)->unique()->nullable();
                $table->string('app_id', 256)->unique()->nullable();
                $table->integer('group_id');
                $table->string('full_name', 256);
                $table->string('phone', 16);
                $table->string('avatar_url', 256);
                $table->integer('title_id');
                $table->integer('birth_day');
                $table->integer('class');
                $table->integer('school_id');
                $table->integer('revenue');
                $table->integer('main_money');
                $table->date('lock_gift_money');
                $table->integer('total_income');
                $table->integer('discount');
                $table->boolean('fixed_discount');
                $table->string('note_1', 512);
                $table->string('note_2', 512);
                $table->integer('status_id');
                $table->string('block_reason', 512);
                $table->string('ip', 64);
                $table->boolean('prioritize_member');
                $table->string('notifi_popup', 2048);
                $table->boolean('view_notifi_popup');
                $table->string('pay_number', 64);
                $table->string('pay_name', 256);
                $table->integer('pay_bank_id');
                $table->integer('pay_bank_agency_id');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
