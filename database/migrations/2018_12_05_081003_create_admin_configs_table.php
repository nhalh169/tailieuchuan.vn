<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\AdminConfigsModel;

class CreateAdminConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(BaseModel::CONNECTION)
            ->create(AdminConfigsModel::table(), function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 128)->unique();
                $table->string('config_key', 256);
                $table->string('config_value', 256)->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
