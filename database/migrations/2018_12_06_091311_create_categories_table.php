<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\CategoriesModel;

class CreateCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection(BaseModel::CONNECTION)
			->create(CategoriesModel::table(), function (Blueprint $table) {
				$table->increments('id');
				$table->string('name', 128);
				$table->integer('parent_id')->default(0);
				$table->integer('order')->default(0);
				$table->string('description', 256)->nullable();
				$table->string('creator', 256)->nullable();
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
