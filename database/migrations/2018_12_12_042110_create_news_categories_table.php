<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\NewsCategoriesModel;

class CreateNewsCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection( BaseModel::CONNECTION )
			->create(NewsCategoriesModel::table(), function (Blueprint $table) {
				$table->increments('id');
				$table->string('name', 256);
				$table->string('description', 1024);
				$table->integer('order');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
