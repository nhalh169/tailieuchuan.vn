<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\NewsesModel;

class CreateNewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection( BaseModel::CONNECTION )
			->create(NewsesModel::table(), function (Blueprint $table) {
				$table->increments('id');
				$table->string('name', 256);
				$table->string('avatar', 256);
				$table->mediumText('description');
				$table->mediumText('content');
				$table->mediumText('key_word');
				$table->string('creator', 128);
				$table->string('approver', 128);
				$table->boolean('hightlight_on_ homepage');
				$table->boolean('hightlight_on_ category');
				$table->integer('order');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
