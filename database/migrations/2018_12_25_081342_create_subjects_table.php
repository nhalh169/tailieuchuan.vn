<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\SubjectsModel;

class CreateSubjectsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection( BaseModel::CONNECTION )
			->create(SubjectsModel::table(), function (Blueprint $table) {
				$table->increments('id');
				$table->string('name', 256);
				$table->integer('category_id');
				$table->string('description', 1024)->nullable();
				$table->integer('order')->nullable();
				$table->string('creator', 128)->nullable();
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
