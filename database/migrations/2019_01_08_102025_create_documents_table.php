<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\BaseModel;
use App\Model\DocumentsModel;

class CreateDocumentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection( BaseModel::CONNECTION )
			->create(DocumentsModel::table(), function (Blueprint $table) {
				$table->increments('id');
				$table->string('name', 256);
				$table->integer('category_id');
				$table->string('link_file', 256)->nullable();
				$table->string('doc_type', 256)->nullable();
				$table->string('size', 128)->nullable();
				$table->integer('price');
				$table->integer('page_count');
				$table->integer('creator_id');
				$table->integer('approver_id');
				$table->string('note', 256)->nullable();
				$table->string('key_word', 256)->nullable();
				$table->boolean('hightlight_in_category')->default(0);
				$table->boolean('hightlight_in_collection')->default(0);
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
