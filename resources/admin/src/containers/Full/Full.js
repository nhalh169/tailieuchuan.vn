import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';

import Colors from '../../views/Theme/Colors/';
import Typography from '../../views/Theme/Typography/';

import Charts from '../../views/Charts/';
import Widgets from '../../views/Widgets/';

// Base
import Cards from '../../views/Base/Cards/';
import Forms from '../../views/Base/Forms/';
import Switches from '../../views/Base/Switches/';
import Tables from '../../views/Base/Tables/';
import Tabs from '../../views/Base/Tabs/';
import Breadcrumbs from '../../views/Base/Breadcrumbs/';
import Carousels from '../../views/Base/Carousels/';
import Collapses from '../../views/Base/Collapses/';
import Dropdowns from '../../views/Base/Dropdowns/';
import Jumbotrons from '../../views/Base/Jumbotrons/';
import ListGroups from '../../views/Base/ListGroups/';
import Navbars from '../../views/Base/Navbars/';
import Navs from '../../views/Base/Navs/';
import Paginations from '../../views/Base/Paginations/';
import Popovers from '../../views/Base/Popovers/';
import ProgressBar from '../../views/Base/ProgressBar/';
import Tooltips from '../../views/Base/Tooltips/';

// Buttons
import Buttons from '../../views/Buttons/Buttons/';
import ButtonDropdowns from '../../views/Buttons/ButtonDropdowns/';
import ButtonGroups from '../../views/Buttons/ButtonGroups/';
import SocialButtons from '../../views/Buttons/SocialButtons/';

// Icons
import Flags from '../../views/Icons/Flags/';
import FontAwesome from '../../views/Icons/FontAwesome/';
import SimpleLineIcons from '../../views/Icons/SimpleLineIcons/';

// Notifications
import Alerts from '../../views/Notifications/Alerts/';
import Badges from '../../views/Notifications/Badges/';
import Modals from '../../views/Notifications/Modals/';

//custom page
// user page
import UserPage                         from '../../views/User/UserPage';
import EditUserPage                     from '../../views/User/EditUserPage';

// group user page
import GroupUserPage                    from '../../views/GroupUser/GroupUserPage';

// setting new user page
import ConfigNewUserPage                from '../../views/ConfigNewUser/ConfigNewUserPage';

// category page
import CategoryPage                     from '../../views/Category/CategoryPage';

// news page
import NewsCategoryPage                 from '../../views/NewsCategory/NewsCategoryPage';
import NewsPage                         from '../../views/News/NewsPage';
import AddNewsPage                      from '../../views/News/AddNewsPage';

// subject page
import SubjectPage                      from '../../views/Subject/SubjectPage';
import SubjectKeyWordPage               from '../../views/SubjectKeyWord/SubjectKeyWordPage';

// document page
import DocumentPage                     from '../../views/Document/DocumentPage';
import DocumentUnapproverPage           from '../../views/Document/DocumentUnapproverPage';

class Full extends Component {

    render() {
        let baseUrl = '/admin';
        let nav = [
            {
                name: 'Dashboard',
                url: baseUrl + '/dashboard',
                icon: 'icon-speedometer',
                badge: {
                    variant: 'info',
                    text: 'NEW'
                }
            },
            {
                name: 'Thành viên',
                url: '/admin/user',
                icon: 'fa fa-user',
                children : [
                    {
                        name: 'Thành viên',
                        url: '/admin/user/manager',
                        icon: 'fa fa-user',
                    },
                    {
                        name: 'Nhóm thành viên',
                        url: '/admin/user/group-user',
                        icon: 'fa fa-users',
                    },
                    {
                        name: 'Cài đặt thành viên',
                        url: '/admin/user/config-new-user',
                        icon: 'fa fa-cog',
                    },
                ]
            },
            {
                name: 'Tài liệu',
                url: '/admin/document',
                icon: 'fa fa-sliders',
                children : [
                    {
                        name: 'Tài liệu',
                        url: '/admin/document/manager',
                        icon: 'fa fa-file-text',
                    },
                    {
                        name: 'Tài liệu chưa duyệt',
                        url: '/admin/document/document-unapprover',
                        icon: 'fa fa-file-o',
                    },
                    {
                        name: 'Bình luận',
                        url: '/admin/document/comment',
                        icon: 'fa fa-commenting-o',
                    },
                    {
                        name: 'Báo lỗi',
                        url: '/admin/document/error-notification',
                        icon: 'fa fa-minus-circle',
                    },
                ]
            },
            {
                name: 'Danh mục',
                url: '/admin/category',
                icon: 'fa fa-sliders',
                children : [
                    {
                        name: 'Danh Mục',
                        url: '/admin/category/manager',
                        icon: 'fa fa-sliders',
                    },
                    {
                        name: 'Chủ đề',
                        url: '/admin/category/subject',
                        icon: 'fa fa-th-large',
                    },
                    {
                        name: 'Từ khóa',
                        url: '/admin/category/key-word',
                        icon: 'fa fa-hashtag',
                    },
                ]
            },
            {
                name: 'Tin tức',
                url: '/admin/news',
                icon: 'fa fa-newspaper-o',
                children : [
                    {
                        name: 'Tin tức',
                        url: '/admin/news/manager',
                        icon: 'fa fa-newspaper-o',
                    },
                    {
                        name: 'Duyệt tin tức',
                        url: '/admin/news/approve',
                        icon: 'fa fa-comments-o',
                    },
                    {
                        name: 'Thêm tin tức',
                        url: '/admin/news/add',
                        icon: 'fa fa-plus',
                    },
                    {
                        name: 'Danh Mục Tin',
                        url: '/admin/news/news-category',
                        icon: 'fa fa-sliders',
                    },
                ]
            },
        ]
        return (
            <div className="app">
                <Header/>
                <div className="app-body">
                    <Sidebar {...this.props} nav = {nav}/>
                    <main className="main">
                        {/*<Breadcrumb/>*/}
                        <br/>
                        <Container fluid>
                            <Switch>
                                {/*User router*/}
                                <Route
                                    exact path={"/admin/user/:id/edit"}
                                    name="EditUserPage"
                                    component={EditUserPage}/>
                                <Route
                                    exact path={"/admin/user/manager"}
                                    name="UserPage"
                                    component={UserPage}/>
                                <Route
                                    exact path={"/admin/user/group-user"}
                                    name="GroupUserPage"
                                    component={GroupUserPage}/>
                                <Route
                                    exact path={"/admin/user/config-new-user"}
                                    name="ConfigNewUserPage"
                                    component={ConfigNewUserPage}/>
                                {/*Category router*/}
                                <Route
                                    exact path={"/admin/category/manager"}
                                    name="CategoryPage"
                                    component={CategoryPage}/>
                                <Route
                                    exact path={"/admin/category/subject"}
                                    name="subject"
                                    component={SubjectPage}/>
                                <Route
                                    exact path={"/admin/category/key-word"}
                                    name="word"
                                    component={SubjectKeyWordPage}/>
                                {/*News router*/}
                                <Route
                                    exact path={"/admin/news/manager"}
                                    name="NewsPage"
                                    component={NewsPage}/>
                                <Route
                                    exact path={"/admin/news/approve"}
                                    name="NewsPage2"
                                    component={NewsPage}/>
                                <Route
                                    exact path={"/admin/news/news-category"}
                                    name="NewsCategoryPage"
                                    component={NewsCategoryPage}/>
                                <Route
                                    exact path={"/admin/news/add"}
                                    name="AddNewsPage"
                                    component={AddNewsPage}/>
                                <Route
                                    exact path={"/admin/news/:id/edit"}
                                    name="EditNewsPage"
                                    component={AddNewsPage}/>
                                {/*Document router*/}
                                <Route
                                    exact path={"/admin/document/manager"}
                                    name="CategoryPage"
                                    component={DocumentPage}/>
                                <Route
                                    exact path={"/admin/document/document-unapprover"}
                                    name="DocumentUnapproverPage"
                                    component={DocumentUnapproverPage}/>
                                <Route
                                    exact path={"/admin/document/comment"}
                                    name="comment"
                                    component={DocumentPage}/>
                                <Route
                                    exact path={"/admin/document/error-notification"}
                                    name="notification"
                                    component={DocumentPage}/>


                                <Route path="/theme/colors" name="Colors" component={Colors}/>
                                <Route path="/theme/typography" name="Typography" component={Typography}/>
                                <Route path="/admin/base/cards" name="Cards" component={Cards}/>
                                <Route path="/base/forms" name="Forms" component={Forms}/>
                                <Route path="/base/switches" name="Swithces" component={Switches}/>
                                <Route path="/base/tables" name="Tables" component={Tables}/>
                                <Route path="/base/tabs" name="Tabs" component={Tabs}/>
                                <Route path="/base/breadcrumbs" name="Breadcrumbs" component={Breadcrumbs}/>
                                <Route path="/base/carousels" name="Carousels" component={Carousels}/>
                                <Route path="/base/collapses" name="Collapses" component={Collapses}/>
                                <Route path="/base/dropdowns" name="Dropdowns" component={Dropdowns}/>
                                <Route path="/base/jumbotrons" name="Jumbotrons" component={Jumbotrons}/>
                                <Route path="/base/list-groups" name="ListGroups" component={ListGroups}/>
                                <Route path="/base/navbars" name="Navbars" component={Navbars}/>
                                <Route path="/base/navs" name="Navs" component={Navs}/>
                                <Route path="/base/paginations" name="Paginations" component={Paginations}/>
                                <Route path="/base/popovers" name="Popovers" component={Popovers}/>
                                <Route path="/base/progress-bar" name="Progress Bar" component={ProgressBar}/>
                                <Route path="/base/tooltips" name="Tooltips" component={Tooltips}/>
                                <Route path="/buttons/buttons" name="Buttons" component={Buttons}/>
                                <Route path="/buttons/button-dropdowns" name="ButtonDropdowns" component={ButtonDropdowns}/>
                                <Route path="/buttons/button-groups" name="ButtonGroups" component={ButtonGroups}/>
                                <Route path="/buttons/social-buttons" name="Social Buttons" component={SocialButtons}/>
                                <Route path="/icons/flags" name="Flags" component={Flags}/>
                                <Route path="/icons/font-awesome" name="Font Awesome" component={FontAwesome}/>
                                <Route path="/icons/simple-line-icons" name="Simple Line Icons" component={SimpleLineIcons}/>
                                <Route path="/notifications/alerts" name="Alerts" component={Alerts}/>
                                <Route path="/notifications/badges" name="Badges" component={Badges}/>
                                <Route path="/notifications/modals" name="Modals" component={Modals}/>
                                <Route path="/widgets" name="Widgets" component={Widgets}/>
                                <Route path="/charts" name="Charts" component={Charts}/>
                                <Route exact path={baseUrl} name="Dashboard" component={Dashboard}/>
                                <Redirect from="/admin" to="/admin"/>
                            </Switch>
                        </Container>
                    </main>
                    <Aside/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Full;
