/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 23/10/2018
 * Modified at: 23/10/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/
import React, {Component}           from 'react';
import Popup                        from 'react-popup';
import axios                        from 'axios';
// import { Parser }                   from 'expr-eval';

const DATA = [];
export default class BaseComponent extends Component
{

    constructor(props) {
        super(props);
        this.state = {
        };

        this.loadingPopup =
            Popup.register({
                className: 'loading-popup',
                content:
                    <div className="lds-facebook">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>,
                closeOnOutsideClick: false,
            });

        this.reset                  = this.reset.bind(this);
        this.loadData               = this.loadData.bind(this);
        this.putData                = this.putData.bind(this);
        this.postData               = this.postData.bind(this);
        this.deleteData             = this.deleteData.bind(this);
    }

    reset(){
        this.setState({reset: ! this.state.reset});
    }

    chunk(str)
    {
        if (str == null || str == '') {
            return 0;
        }
        str = parseInt(str);
        if(typeof(str) != "number") {
            return 0;
        }
        let minus = "";
        if (str < 0) {
            minus = "-";
            str = -str;
        }
        str = str.toString();
        let ret = [];
        let i;
        let n = 3;
        let len = str.length;

        for(i = len - n; i > -n; i -= n) {
            if (i > 0) {
                ret.unshift(str.substr(i, n));
            } else {
                ret.unshift(str.substr(0, n + i));
                return minus + ret.toString();
            }
        }
        return 0;
    }

    renderShowsTotal(start, to, total)
    {
        return (
            <span>
                From { start } to { to }, Total rows: { total } {" "}
            </span>
        );
    }

    createCustomToolBar(props)
    {
        return (
            <div className='col-sm-12 row'>
                { props.components.btnGroup }
                <div className='col-xs-8 col-sm-4 col-md-4 col-lg-2'>
                    { props.components.searchPanel }
                </div>
            </div>
        );
    }

    getDefaultOptions(max = 100, otherOptions = {})
    {
        let options = {
            page: 1,  // which page you want to show as default
            paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
            sizePerPageList: [ {
                text: '10 Rows', value: 10
            }, {
                text: '50 Rows', value: 50
            }, {
                text: 'All Rows', value: max
            } ], // you can change the dropdown list for size per page
            sizePerPage: 50,  // which size per page you want to locate as default
            pageStartIndex: 1, // where to start counting the pages
            paginationSize: 5,  // the pagination bar size.
            prePage: 'Prev', // Previous page button text
            nextPage: 'Next', // Next page button text
            firstPage: 'First', // First page button text
            lastPage: 'Last', // Last page button text
            paginationPosition: 'bottom',  // default is bottom, top and both is all available
            toolBar: this.createCustomToolBar,
            ...otherOptions,
        };
        return options;

    }

    productFormatter(cell, row, enumObject)
    {
        return enumObject[cell];
    }

    async loadData(link = '', reload = false, reloadTime = 3)
    {
        let action = link.substring(0, link.indexOf("?"));
        if(!reload && (DATA[action] || DATA[link])) {
            return DATA[link] ? DATA[link] : DATA[action] ;
        }
        let result = [];
        if (!reload) {
            Popup.queue(this.loadingPopup);
        }
        return await axios.get(link)
            .then(
                res => {
                    if (!reload) {
                        Popup.close();
                    }
                    if (res.data.success) {
                        result = res.data.result;
                        DATA[link] = result;
                        return new Promise(
                            (resolve) => {
                                resolve(result);
                            }
                        );
                    } else {
                        if(reloadTime > 0) {
                            return this.loadData(link, reload, -- reloadTime);
                        } else {
                            Popup.alert(res.data.message);
                            return new Promise(
                                (resolve) => {
                                    resolve(result);
                                }
                            );
                        }
                    }
                }
            ).catch(
                e => {
                    Popup.close();
                    console.log(e);
                    if(reloadTime > 0) {
                        return this.loadData(link, reload, -- reloadTime);
                    } else {
                        Popup.alert(e.toString());
                        return new Promise(
                            (resolve) => {
                                resolve(result);
                            }
                        );
                    }
                }
            );
    }

    async postData(url, data) {
        Popup.queue(this.loadingPopup);
        return await axios.post(url, data)
            .then(
                res => {
                    Popup.close();
                    if (res.data.success) {
                        return res.data.result;
                    } else {
                        Popup.alert(res.data.message);
                        return false;
                    }
                }
            ) .catch(
                e => {
                    Popup.close();
                    console.log(e);
                    Popup.alert(e.toString());
                    return false;
                }
            )
    }

    async putData(url, data)
    {
        Popup.queue(this.loadingPopup);
        return await axios.put(url, data)
            .then(
                res => {
                    Popup.close();
                    if (res.data.success) {
                        return res.data.result;
                    } else {
                        Popup.alert(res.data.message);
                        return false;
                    }
                }
            ) .catch(
                e => {
                    Popup.close();
                    console.log(e);
                    Popup.alert(e.toString());
                    return false;
                }
            )
    }

    async deleteData(url)
    {
        Popup.queue(this.loadingPopup, true);
        return await axios.delete(url)
            .then(
                res => {
                    Popup.close();
                    if (res.data.success) {
                        return res.data.result;
                    } else {
                        Popup.alert(res.data.message);
                        return false;
                    }
                }
            ) .catch(
                e => {
                    Popup.close();
                    console.log(e);
                    Popup.alert(e.toString());
                    return false;
                }
            )
    }

}
