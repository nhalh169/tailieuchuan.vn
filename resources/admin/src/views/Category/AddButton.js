/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 05/06/2018
 * Modified at: 05/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import ModalInput                   from './ModalInput';

export default class AddButton extends Component
{
    constructor(props) {
        super(props);
        this.state = {
        };
        this.toggleModal = this.toggleModal.bind(this);
    }

    toggleModal(){
        this.setState({showModal : !this.state.showModal});
    }

    render() {
        return (
            <div className="d-sm-inline-block ml-2">
                <button
                    className= "btn btn-sm btn-success"
                    onClick={this.toggleModal}>
                    <i className="fa fa-plus"></i> {' Thêm mới '}
                </button>
                {
                    this.state.showModal &&
                    <ModalInput
                        reloadListData={ this.props.reloadListData }
                        showModal={this.state.showModal }
                        toggleModal={this.toggleModal} />
                }
            </div>
        );
    }
}