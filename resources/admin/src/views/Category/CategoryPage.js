/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import AddChildButton               from './AddChildButton';
import EditButton                   from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

const maxLevel = 20;
const defaultShowLevel = 1;
export default class CategoryPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listCategory : [],
            treeCategory : [],
        }

        this.listData = [
            {
                link : "/admin/categories",
                state_key :"listCategory"
            },
        ];

        this.reloadListData             = this.reloadListData.bind(this);
        this.validateDeleteCategory     = this.validateDeleteCategory.bind(this);
        this.generateTreeCategory            = this.generateTreeCategory.bind(this);
        this.getChildCategory           = this.getChildCategory.bind(this);
        this.renderTreeCategoryTable    = this.renderTreeCategoryTable.bind(this);
        // this.deleteCategory            = this.deleteCategory.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        let { treeCategory } = this.state;
        this.generateTreeCategory();
        // this.reset();
    }

    async reloadListData()
    {
        let { listCategory } = this.state;
        let newListCategory = await this.loadData('/admin/categories', true);
        newListCategory.forEach(
            newCate => {
                let cate = listCategory.find(n => n.id == newCate.id);
                if(cate) {
                    newCate.showChildStatus = cate.showChildStatus;
                }
            }
        )
        this.state.listCategory = newListCategory;
        this.generateTreeCategory();
        // this.reset();
    }

    getChildCategory(category)
    {
        let { listCategory } = this.state;
        let child = listCategory.filter(c => c.parent_id == category.id);
        child.sort(
            (cateA, cateB) => {
                return cateA.order - cateB.order;
            }
        )
        child.forEach(
            cate => {
                cate.level = category.level + 1;
                cate.child = this.getChildCategory(cate);
            }
        )
        return child;
    }

    generateTreeCategory()
    {
        let { listCategory } = this.state;
        let treeCategory = listCategory.filter(c => c.parent_id == 0);
        treeCategory.sort(
            (cateA, cateB) => {
                return cateA.order - cateB.order;
            }
        )
        treeCategory.forEach(
            cate => {
                //create new object without dupplicate object in list Category
                cate.level = 1;
                cate.child = this.getChildCategory(cate);
            }
        )
        this.setState({ treeCategory: treeCategory });
    }

    validateDeleteCategory(category)
    {
        if(category && (!category.count_document || category.count_document == 0)) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa danh mục ?',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa danh mục ' + category.name + ' ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData("/admin/categories/" + category.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        } else {
            Popup.alert('Danh mục tồn tại tài liệu! Vui lòng kiểm tra lại');
            return;
        }
    }

    hideChildCategory(category)
    {
        category.showChildStatus = 0;
        category.child.forEach(
            cate => {
                this.hideChildCategory(cate);
            }
        )
    }

    renderCategoryRow(category)
    {
        let listRow = [];
        let levelDistance = [];
        levelDistance.push(<td key={ 'level-distance-' + category.id} colSpan={category.level}></td>);
        let showButton = [];
        if(category.child.length > 0 && category.level >= defaultShowLevel) {
            if(category.showChildStatus != 1) {
                showButton.push(
                    <button
                        key={"plus-button" + category.id}
                        className="btn btn-sm btn-success ml-2"
                        onClick={ ()=> {category.showChildStatus = 1; this.reset()}}
                    >
                        <i className="fa fa-plus"></i>
                    </button>
                )
            } else {
                showButton.push(
                    <button
                        key={"minus-button" + category.id}
                        className="btn btn-sm btn-primary ml-2"
                        onClick={ ()=> {this.hideChildCategory(category); this.reset();}}
                    >
                        <i className="fa fa-minus"></i>
                    </button>
                )

            }
        }
        listRow.push(
            <tr key={ 'row' + category.id }>
                <td>{category.id}</td>
                {levelDistance}
                <td colSpan={maxLevel - category.level}>{ category.name } { showButton }</td>
                <td>{ category.order }</td>
                <td style={{width: '300px'}}>
                    <AddChildButton category={ category } reloadListData={ this.reloadListData }/>
                    <EditButton category={ category } reloadListData={ this.reloadListData }/>
                    <button
                        className="btn btn-sm btn-danger ml-2"
                        onClick={ () => this.validateDeleteCategory(category) }
                    >
                        <i className="fa fa-trash"></i>{' Xóa '}
                    </button>
                </td>
                <td>{ category.count_subject }</td>
                <td>{ category.count_document }</td>
                <td>{ category.count_view }</td>
                <td>{ category.count_download }</td>
                <td>{ category.count_revenue }</td>
                <td>{ category.count_money }</td>
                <td>{ category.creator }</td>
                <td>{ category.created_at}</td>
            </tr>
        )
        if(category.level < defaultShowLevel || category.showChildStatus) {
            category.child.forEach(
                cate => {
                    listRow.push(this.renderCategoryRow(cate))
                }
            )
        }
        return listRow;
    }

    renderTreeCategoryTable()
    {
        let {
            treeCategory,
        } = this.state;
        let listRow = [];
        treeCategory.forEach(
            cate => {
                listRow.push(this.renderCategoryRow(cate));
            }
        )
        let defaultCol = [];
        for(let i = 0; i < maxLevel + 11; i++) {
            defaultCol.push(<td key={i}></td>)
        }
        return(
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th></th>
                    <th colSpan={maxLevel - 1}>{'Tên danh mục'}</th>
                    <th>{'Thứ tự'}</th>
                    <th>
                        <label style={ { width: '200px' } }>{ 'Thao tác' }</label>
                    </th>
                    <th>{'Số chủ đề'}</th>
                    <th>{'Số tài liệu'}</th>
                    <th>{'Lượt xem'}</th>
                    <th>{'Lượt tải'}</th>
                    <th>{'Doang thu'}</th>
                    <th>{'Tổng tiền'}</th>
                    <th>{'Người tạo'}</th>
                    <th>
                        <label style={ { width: '200px' } }>{ 'Ngày tạo' }</label>
                    </th>
                </tr>
                </thead>
                <tbody>
                {listRow}
                <tr>
                    {defaultCol}
                </tr>
                </tbody>
            </table>
        );
    }

    render() {
        let {
            listCategory,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label className="mr-2">{ 'Danh mục ' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        {this.renderTreeCategoryTable()}
                    </div>
                </div>
            </div>
        );
    }
}

