/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 20/06/2018
 * Modified at: 20/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


import React, { Component }     from 'react';
import Popup                    from 'react-popup';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
}                               from 'reactstrap';
import BaseComponent            from '../BaseComponent';

export default class ModalInput extends BaseComponent
{
    constructor(props)
    {
        super(props);
        let defaultCategory = {
            name: '',
            parent_id: 0,
            order: 1,
        }
        console.log(this.props.category);
        this.state = {
            category : this.props.category ? {...this.props.category}: defaultCategory,
            listCategory: [],
        };

        this.listData = [
            {
                link: '/admin/categories',
                state_key: 'listCategory'
            }
        ];
        this.submitApi = '/admin/categories';

        this.validateData                       = this.validateData.bind(this);
        this.submit                             = this.submit.bind(this);
        this.renderSelectParentCategory         = this.renderSelectParentCategory.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    // async reloadListData()
    // {
    //     this.state.listCategory = await this.loadData('/admin/categories', true);
    //     this.reset();
    // }

    validateData()
    {
        let { category } = this.state;
        if(!category.name || category.name.length == 0) {
            Popup.alert("Vui lòng điền tên nhóm thành viên!");
            return;
        }
        if(category.name.length > 200) {
            Popup.alert("Tên nhóm thành viên quá dài!");
            return;
        }
        this.submit();
    }

    async submit()
    {
        let { category } = this.state;
        let result = false;
        if(category.id) {
            result = await this.putData(this.submitApi + "/" + category.id, category);
        } else {
            result = await this.postData(this.submitApi, category);
        }
        if(result != false) {
            Popup.alert('Cập nhật thông tin thành công!');
            await this.props.reloadListData();
            // if(! category.id) {
            //     this.props.toggleModal();
            // }
            this.props.toggleModal();
            this.props.reloadListData();
        }
    }

    renderSelectParentCategory(selectCategory)
    {
        let { listCategory, category } = this.state;
        let listSelectCategory = [];
        if(!selectCategory || !selectCategory.parent_id || selectCategory.parent_id == 0) {
            let firstCategory = listCategory.filter(c => c.parent_id == 0);
            let categoryOptions = [];
            categoryOptions.push(
                <option value={0} key={0}>{'Chọn danh mục cha'}</option>
            )
            firstCategory.forEach(
                cate => {
                    categoryOptions.push(
                        <option value={cate.id} key={cate.id}>{cate.name}</option>
                    )
                }
            )
            return(
                <select
                    key={ 'select-category-' + selectCategory.id }
                    className="form-control mb-2"
                    defaultValue={ selectCategory.id }
                    onChange={ e => { category.parent_id = e.target.value; this.reset(); } }
                >
                    { categoryOptions }
                </select>
            )
        } else {
            let parentCategory = listCategory.find(c => c.id == selectCategory.parent_id);
            if(parentCategory) {
                listSelectCategory.push(this.renderSelectParentCategory(parentCategory));
                let brotherCategory = listCategory.filter(c => c.parent_id == selectCategory.parent_id);
                if(brotherCategory.length != 0) {
                    let categoryOptions = [];
                    categoryOptions.push(
                        <option value={selectCategory.parent_id} key={0}>{'Chọn danh mục'}</option>
                    )
                    brotherCategory.forEach(
                        cate => {
                            categoryOptions.push(
                                <option value={cate.id} key={cate.id}>{cate.name}</option>
                            )
                        }
                    )
                    listSelectCategory.push(
                        <select
                            key={'select-category-' + selectCategory.id}
                            className="form-control mb-2"
                            defaultValue={ selectCategory.id }
                            onChange={ e => { category.parent_id = e.target.value; this.reset(); } }
                        >
                            {categoryOptions}
                        </select>
                    )
                }
            }
        }
        return(
            <div key="list-select-category" className="list-select-category">
                {listSelectCategory}
            </div>
        )
    }

    render()
    {
        let {
            category,
        } = this.state;

        return(
            <Modal
                className="modal-lg"
                isOpen = {this.props.showModal}
                toggle = {this.props.toggleModal}>
                <ModalHeader toggle = {this.props.toggleModal}>
                    {category.id ? ("Sửa danh mục: " + this.state.category.name) : "Tạo danh mục"}
                </ModalHeader>
                <ModalBody>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Danh mục cha'}</label> </div>
                        <div className="col-sm-4">
                            {this.renderSelectParentCategory(category)}
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Tên danh mục'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ this.state.category.name }
                                onChange={ e => { this.state.category.name = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Thứ tự'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ this.state.category.order }
                                onChange={ e => { this.state.category.order = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Mô tả'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ this.state.category.description }
                                onChange={ e => { this.state.category.description = e.target.value } }
                            />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button
                        className="btn btn-success"
                        onClick={ this.submit }>
                        <i className="fa fa-save"></i> {' Lưu '}
                    </button>
                    <button
                        className="btn topica-btn-gray"
                        onClick={ this.props.toggleModal }>
                        <i className="fa fa-close"></i> {' Đóng '}
                    </button>
                </ModalFooter>
            </Modal>
        );
    }
}