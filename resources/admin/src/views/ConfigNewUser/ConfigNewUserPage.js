/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import axios                        from 'axios';
import BaseComponent                from '../BaseComponent';

export default class ConfigNewUserPage extends BaseComponent
{
    constructor(props)
    {
        super(props);
        this.state = {
            configNewUserMoney : {
                name: 'config-new-user-money',
                config_key: 'money',
                config_value: 0,
            },
            configNewUserPopup : {
                name: 'config-new-user-popup',
                config_key: 'popup',
                config_value: '',
            },
        }
        this.listData = [
            {
                link : "/admin/admin-configs/get-config-by-name?name=config-new-user-money",
                state_key :"configNewUserMoney"
            },
            {
                link : "/admin/admin-configs/get-config-by-name?name=config-new-user-popup",
                state_key :"configNewUserPopup"
            },
        ];

        this.submit         = this.submit.bind(this);

    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            let result = await this.loadData(data.link);
            if(result) {
                this.state[data.state_key] = result;
            }
        }
        this.reset();
    }

    async submit()
    {
        let {
            configNewUserMoney,
            configNewUserPopup,
        } = this.state;
        let submitStatus = true;
        if(configNewUserMoney.id) {
            let result = await this.putData('/admin/admin-configs/' + configNewUserMoney.id, configNewUserMoney);
            if (result != 0) {
                this.state.configNewUserMoney = result;
                this.reset();
            } else {
                submitStatus = false;
            }
        } else {
            let result = await this.postData('/admin/admin-configs', configNewUserMoney);
            if (result != 0) {
                this.state.configNewUserMoney = result;
                this.reset();
            } else {
                submitStatus = false;
            }

        }
        if(configNewUserPopup.id) {
            let result = await this.putData('/admin/admin-configs/' + configNewUserPopup.id, configNewUserPopup);
            if (result != 0) {
                this.state.configNewUserPopup = result;
                this.reset();
            } else {
                submitStatus = false;
            }
        } else {
            let result = await this.postData('/admin/admin-configs', configNewUserPopup);
            if (result != 0) {
                this.state.configNewUserPopup = result;
                this.reset();
            } else {
                submitStatus = false;
            }

        }
        if(submitStatus) {
            Popup.alert('Cập nhật thông tin thành công!');
        }

    }

    render()
    {
        let {
            configNewUserMoney,
            configNewUserPopup,
        } = this.state;

        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-header">{'Cài đặt thành viên mới'}</div>
                    <div className="card-body">
                        <div className="row form-group">
                            <div className="col-sm-3"><label> {'Cộng tiền khuyến mãi'}</label> </div>
                            <div className="col-sm-4">
                                <input
                                    type="number"
                                    className="form-control"
                                    autoFocus={ true }
                                    key={ configNewUserMoney.id + this.state.reset }
                                    defaultValue={ configNewUserMoney.config_value }
                                    onChange={
                                        e => {
                                            configNewUserMoney.config_value = Number(e.target.value)
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-3"><label> {'Thông báo Popup'}</label> </div>
                            <div className="col-sm-9">
                                <textarea
                                    className="form-control"
                                    rows="10"
                                    key={ configNewUserMoney.id + this.state.reset }
                                    defaultValue={ configNewUserPopup.config_value }
                                    onChange={
                                        e => {
                                            configNewUserPopup.config_value = e.target.value
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-7">
                                <button
                                    className="btn btn-success float-right"
                                    onClick={ this.submit }
                                >
                                    <i className="fa fa-save"></i>{' Lưu '}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

