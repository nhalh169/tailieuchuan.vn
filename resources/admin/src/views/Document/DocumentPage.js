/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import EditButton                    from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class DocumentPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listDocument : [],
            listCategory : [],
            listSubject : [],
        }
        this.submitApi = '/admin/documents';

        this.listData = [
            {
                link : this.submitApi,
                state_key :"listDocument"
            },
            {
                link : '/admin/categories',
                state_key :"listCategory"
            },
            {
                link : '/admin/subjects',
                state_key :"listSubject"
            },
        ];

        this.reloadListData                 = this.reloadListData.bind(this);
        this.validateDeleteDocument         = this.validateDeleteDocument.bind(this);
        this.getMapOfCategory               = this.getMapOfCategory.bind(this);
        this.renderListSubject              = this.renderListSubject.bind(this);
        // this.deleteDocument            = this.deleteDocument.bind(this);
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listDocument = await this.loadData(this.submitApi, true);
        this.reset();
    }

    renderListSubject(subjectIds)
    {
        if(!subjectIds) {
            return;
        }
        let { listSubject } = this.state;
        let listSubId = subjectIds.split(',');
        let listSubName = [];
        listSubId.forEach(
            subId => {
                let sub = listSubject.find(s => s.id == subId);
                if(sub) {
                    listSubName.push(sub.name)
                } else {
                    listSubName.push(subId)
                }
            }
        )
        return listSubName.join(', ');
    }

    validateDeleteDocument(subject)
    {
        // if(subject && subject.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa tài liệu',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa tài liệu ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData(this.submitApi + '/' + subject.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        // } else {
        //     Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
        //     return;
        // }
    }

    getMapOfCategory(category_id)
    {
        let { listCategory } = this.state;
        let cate = listCategory.find(c => c.id == category_id);
        if(cate) {
            let result = cate.name;
            let parentCate = listCategory.find(c => c.id == cate.parent_id);
            if(parentCate) {
                result = this.getMapOfCategory(parentCate.id) + ' -> ' + result;
            }
            return result;
        } else {
            console.log("Can not find cate: " + category_id);
        }
    }

    render() {
        let {
            listDocument,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Tài liệu' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listDocument }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listDocument.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'70'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tên tài liệu' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='doc_type'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Dạng file' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='size'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Dung lượng' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='size'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Giá' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='size'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số trang' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='category_id'
                                dataSort
                                dataTitle
                                width={'250'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    cell => {
                                        return this.getMapOfCategory(cell);
                                    }
                                }
                                filterFormatted
                            >
                                { 'Danh mục' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='subject_ids'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    cell => this.renderListSubject(cell)
                                }
                                filterFormatted
                            >
                                { 'Chủ đề' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='description'
                                dataAlign='right'
                                dataSort
                                width={'200'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Mô tả' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_news'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số tin xem' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số lượt tải' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số lượt bình luận' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='creator_id'
                                dataSort
                                width="150"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Người đăng' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='approver_id'
                                dataSort
                                width="150"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Người duyệt' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='status'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Trạng thái' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='note'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Ghi chú' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='hightlight_in_category'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Nổi bật DM' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='hightlight_in_subject'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Nổi bật chủ đề' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <EditButton
                                                    existData={row}
                                                    reloadListData={ this.reloadListData }
                                                />
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteDocument(row)}
                                                > <i className="fa fa-trash"></i> {'Xóa'}</button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

