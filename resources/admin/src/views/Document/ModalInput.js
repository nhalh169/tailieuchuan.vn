/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 20/06/2018
 * Modified at: 20/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


import React, { Component }     from 'react';
import Popup                    from 'react-popup';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    FormGroup,
    Label,
    Input,
}                               from 'reactstrap';
import BaseComponent            from '../BaseComponent';
import Select                       from 'react-select'
export default class ModalInput extends BaseComponent
{
    constructor(props)
    {
        super(props);

        this.state = {
            document : this.props.existData ? {...this.props.existData}: {},
            listCategory : [],
            listSubject : [],
            listUser : [],
        };

        this.listData = [
            {
                link: '/admin/categories',
                state_key: 'listCategory'
            },
            {
                link: '/admin/subjects',
                state_key: 'listSubject'
            },
            {
                link: '/admin/users',
                state_key: 'listUser'
            },
        ];

        this.submitApi = '/admin/documents';

        this.validateAndSubmit               = this.validateAndSubmit.bind(this);
        this.submit                     = this.submit.bind(this);
        this.renderSelectCategory       = this.renderSelectCategory.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
    }

    validateAndSubmit()
    {
        let { listCategory, document } = this.state;
        let childCate = listCategory.filter(c => c.parent_id == document.category_id);
        if(childCate.length > 0) {
            Popup.alert("Danh mục này chứa danh mục con. Vui lòng chọn danh mục khác !");
            return;

        }
        if(!document.name || document.name.length == 0) {
            Popup.alert("Vui lòng điền tên tài liệu!");
            return;
        }
        if(document.name.length > 200) {
            Popup.alert("Tên chủ đề quá dài!");
            return;
        }
        if(!document.price || document.price == 0) {
            Popup.alert("Vui lòng điền giá tài liệu!");
            return;
        }
        this.submit();
    }

    async submit()
    {
        let { document } = this.state;
        let result = false;
        if(document.id) {
            result = await this.putData(this.submitApi + "/" + document.id, document);
        } else {
            result = await this.postData(this.submitApi, document);
        }
        if(result != false) {
            Popup.alert('Cập nhật thông tin thành công!');
            this.props.reloadListData();
            this.props.toggleModal();
        }
    }

    renderSelectCategory(categoryId)
    {
        let { listCategory, document } = this.state;
        let listSelectCategory = [];
        let category = listCategory.find(c => c.id == categoryId);
        if(!category || categoryId == 0) {
            let firstCategories = listCategory.filter(c => c.parent_id == 0);
            let categoryOptions = [];
            categoryOptions.push(
                <option value={0} key={0}>{'Chọn danh mục'}</option>
            )
            firstCategories.forEach(
                cate => {
                    categoryOptions.push(
                        <option value={cate.id} key={cate.id}>{cate.name}</option>
                    )
                }
            )
            return(
                <select
                    key={'select-category-' + categoryId}
                    className="form-control mb-2"
                    defaultValue={ document.category_id }
                    onChange={ e => {
                        document.category_id = e.target.value;
                        document.subject_ids = '';
                        this.reset();
                    } }
                >
                    {categoryOptions}
                </select>
            )

        }
        let parentCategory = listCategory.find(c => c.id == category.parent_id);
        // render parent category
        if(parentCategory && category.parent_id != document.category_id) {
            listSelectCategory.push(this.renderSelectCategory(parentCategory.id));
        }

        let brotherCategory = listCategory.filter(c => c.parent_id == category.parent_id);
        let brotherCategoryOptions = [];
        brotherCategoryOptions.push(
            <option value={category.parent_id} key={0}>{'Chọn danh mục'}</option>
        )
        brotherCategory.forEach(
            cate => {
                brotherCategoryOptions.push(
                    <option value={cate.id} key={cate.id}>{cate.name}</option>
                )
            }
        )
        listSelectCategory.push(
            <select
                key={ 'select-category-' + categoryId }
                className="form-control mb-2"
                defaultValue={ category.parent_id == document.category_id ? '' : categoryId }
                onChange={ e => {
                    document.category_id = e.target.value;
                    document.subject_ids = '';
                    this.reset();
                } }
            >
                {brotherCategoryOptions}
            </select>
        )

        let childCategory = listCategory.filter(c => c.parent_id == category.id);
        // render child category
        if(childCategory.length > 0 && category.id == document.category_id) {
            listSelectCategory.push(this.renderSelectCategory(childCategory[0].id));
        }
        return listSelectCategory;
        return(
            <div key="list-select-category" className="list-select-category">
                {listSelectCategory}
            </div>
        )
    }

    render()
    {
        let {
            document,
            listSubject,
            listCategory,
            listUser,
        } = this.state;
        let subjectOptions = [];
        listSubject.forEach(
            subject => {
                if (document.category_id == subject.category_id) {
                    subjectOptions.push({label: subject.name, value: subject.id})
                }
            }
        )
        let userOptions = [];
        userOptions.push(
            <option value={ '' } key={ 'default' }>{ 'Chọn người tạo' }</option>
        )
        listUser.forEach(
            user => {
                userOptions.push(
                    <option value={ user.id } key={ user.id }>{ user.email }</option>
                )
            }
        )
        return(
            <Modal
                className="modal-lg"
                isOpen = {this.props.showModal}
                toggle = {this.props.toggleModal}>
                <ModalHeader toggle = {this.props.toggleModal}>
                    {document.id ? ("Sửa chủ đề: " + this.state.document.name) : "Tạo tài liệu"}
                </ModalHeader>
                <ModalBody>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Danh mục'}</label> </div>
                        <div className="col-sm-4">
                            { this.renderSelectCategory(document.category_id) }
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Chủ đề'}</label> </div>
                        <div className="col-sm-9">
                            <Select
                                multi
                                simpleValue
                                removeSelected={true}
                                closeOnSelect ={false}
                                options={ subjectOptions }
                                placeholder="Chọn chủ đề"
                                onChange={
                                    value => {
                                        document.subject_ids = value;
                                        this.reset();
                                    }
                                }
                                value={ document.subject_ids }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Tên tài liệu'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ this.state.document.name }
                                onChange={ e => { this.state.document.name = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Dạng tài liệu'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                defaultValue={ this.state.document.doc_type }
                                placeholder=".doc, pdf, etc"
                                onChange={ e => { this.state.document.doc_type = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Giá'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                type="number"
                                defaultValue={ document.price }
                                onChange={ e => { document.price = e.target.value; this.reset(); } }
                            />
                        </div>
                        {this.chunk(document.price) + ' đồng'}
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Mô tả'}</label> </div>
                        <div className="col-sm-9">
                            <textarea
                                className="form-control"
                                rows={'6'}
                                defaultValue={ this.state.document.description }
                                onChange={ e => { this.state.document.description = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Người tạo (ID)'}</label> </div>
                        <div className="col-sm-9">
                            <select
                                className="form-control"
                                defaultValue={ this.state.document.creator_id }
                                onChange={ e => { this.state.document.creator_id = e.target.value } }
                            >
                                {userOptions}
                            </select>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Người duyệt (ID)'}</label> </div>
                        <div className="col-sm-9">

                            <select
                                className="form-control"
                                defaultValue={ this.state.document.approver_id }
                                onChange={ e => { this.state.document.approver_id = e.target.value } }
                            >
                                {userOptions}
                            </select>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button
                        className="btn btn-success"
                        onClick={ this.validateAndSubmit }>
                        <i className="fa fa-save"></i> {' Lưu '}
                    </button>
                    <button
                        className="btn topica-btn-gray"
                        onClick={ this.props.toggleModal }>
                        <i className="fa fa-close"></i> {' Đóng '}
                    </button>
                </ModalFooter>
            </Modal>
        );
    }
}