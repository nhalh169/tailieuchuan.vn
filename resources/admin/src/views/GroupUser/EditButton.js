/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 05/06/2018
 * Modified at: 05/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import ModalInput                   from './ModalInput';

export default class EditButton extends Component
{
    constructor(props) {
        super(props);
        this.state = {
        };
        this.toggleModal = this.toggleModal.bind(this);
    }

    toggleModal(){
        this.setState({showModal : !this.state.showModal});
    }

    render() {
        return (
            <div className="d-sm-inline-block">
                <button
                    className="btn btn-primary btn-sm"
                    onClick={this.toggleModal}>
                    <i className="fa fa-edit"></i> {' Sửa '}
                </button>
                <ModalInput
                    groupUser={this.props.groupUser}
                    reloadListData={ this.props.reloadListData }
                    showModal={this.state.showModal }
                    toggleModal={this.toggleModal} />
            </div>
        );
    }
}