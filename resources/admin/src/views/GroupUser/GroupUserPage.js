/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import EditButton                    from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class GroupUserPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listGroupUser : [],
            listRole : [],
            listProduct : [],
        }

        this.listData = [
            {
                link : "/admin/group-users/all-data",
                state_key :"listGroupUser"
            },
        ];

        this.reloadListData             = this.reloadListData.bind(this);
        this.validateDeleteGroupUser          = this.validateDeleteGroupUser.bind(this);
        // this.deleteGroupUser            = this.deleteGroupUser.bind(this);
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listGroupUser = await this.loadData('/admin/group-users/all-data', true);
        this.reset();
    }

    validateDeleteGroupUser(groupUser)
    {
        if(groupUser && groupUser.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa nhóm người dùng ?',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa nhóm người dùng ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData("/admin/group-users/" + groupUser.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        } else {
            Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
            return;
        }
    }

    render() {
        let {
            listGroupUser,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Nhóm thành viên' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listGroupUser }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listGroupUser.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tên Nhóm' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='number_user'
                                dataAlign='right'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số Người' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='permission'
                                dataSort
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Quyền hạn' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='created_at'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Ngày tạo' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <EditButton
                                                    groupUser={row}
                                                    reloadListData={ this.reloadListData }
                                                />
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteGroupUser(row)}
                                                > <i className="fa fa-trash"></i> {'Xóa'}</button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

