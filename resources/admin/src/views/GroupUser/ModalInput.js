/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 20/06/2018
 * Modified at: 20/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


import React, { Component }     from 'react';
import Popup                    from 'react-popup';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    FormGroup,
    Label,
    Input,
}                               from 'reactstrap';
import axios                    from 'axios';
import Select                   from 'react-select';
import BaseComponent            from '../BaseComponent';

export default class ModalInput extends BaseComponent
{
    constructor(props)
    {
        super(props);

        this.state = {
            groupUser : this.props.groupUser ? {...this.props.groupUser}: {},
        };

        this.listData = [
            // {
            //     link : "/products",
            //     state_key :"listProduct"
            // },
        ];

        this.submitApi = '/admin/group-users';

        this.validateData               = this.validateData.bind(this);
        this.submit                     = this.submit.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
    }

    validateData()
    {
        let { groupUser } = this.state;
        if(!groupUser.name || groupUser.name.length == 0) {
            Popup.alert("Vui lòng điền tên nhóm thành viên!");
            return;
        }
        if(groupUser.name.length > 200) {
            Popup.alert("Tên nhóm thành viên quá dài!");
            return;
        }
        this.submit();
    }

    async submit()
    {
        let { groupUser } = this.state;
        let result = false;
        if(groupUser.id) {
            result = await this.putData(this.submitApi + "/" + groupUser.id, groupUser);
        } else {
            result = await this.postData(this.submitApi, groupUser);
        }
        if(result != false) {
            Popup.alert('Cập nhật thông tin thành công!');
            this.props.reloadListData();
            this.props.toggleModal();
        }
    }
    render()
    {
        let {
            groupUser,
        } = this.state;
        let listPermisisonObject = [
            {
                id: 1,
                name: 'Quản lý tài liệu'
            },
            {
                id: 2,
                name: 'Quản lý lỗi tài liệu'
            },
            {
                id: 3,
                name: 'Quản lý bình luận'
            },
            {
                id: 4,
                name: 'Quản lý tin tức'
            },
            {
                id: 5,
                name: 'Quản lý tài chính'
            },
        ];
        let objectOptions = [];
        listPermisisonObject.forEach(
            per => {
                objectOptions.push(
                    <div className="row form-group" key={per.id}>
                        <div className="col-sm-3"><label> {per.name}</label> </div>
                        <div className="col-sm-9">
                            <div className="position-relative form-check form-check-inline">
                                <input
                                    id={'per-add-option-' + per.id}
                                    type="checkbox"
                                    className="form-check-input"
                                    value="" />
                                <label
                                    htmlFor={'per-add-option-' + per.id}
                                    className="form-check-label">{'Thêm'}</label>
                            </div>
                            <div className="position-relative form-check form-check-inline">
                                <input
                                    id={'per-approve-option-' + per.id}
                                    type="checkbox"
                                    className="form-check-input"
                                    value="" />
                                <label
                                    htmlFor={'per-approve-option-' + per.id}
                                    className="form-check-label">{'Duyệt'}</label>
                            </div>
                            <div className="position-relative form-check form-check-inline">
                                <input
                                    id={'per-edit-option-' + per.id}
                                    type="checkbox"
                                    className="form-check-input"
                                    value="" />
                                <label
                                    htmlFor={'per-edit-option-' + per.id}
                                    className="form-check-label">{'Sửa'}</label>
                            </div>
                            <div className="position-relative form-check form-check-inline">
                                <input
                                    id={'per-delete-option-' + per.id}
                                    type="checkbox"
                                    className="form-check-input"
                                    value="" />
                                <label
                                    htmlFor={'per-delete-option-' + per.id}
                                    className="form-check-label">{'Xóa'}</label>
                            </div>
                        </div>
                    </div>
                )
            }
        )
        return(
            <Modal
                className="modal-lg"
                isOpen = {this.props.showModal}
                toggle = {this.props.toggleModal}>
                <ModalHeader toggle = {this.props.toggleModal}>
                    {groupUser.id ? ("Sửa nhóm thành viên: " + this.state.groupUser.name) : "Tạo nhóm thành viên"}
                </ModalHeader>
                <ModalBody>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Tên nhóm'}</label> </div>
                        <div className="col-sm-4">
                            <input className="form-control"
                                   autoFocus={ true }
                                   defaultValue={ this.state.groupUser.name }
                                   onChange={ e => { this.state.groupUser.name = e.target.value } }
                            />
                        </div>
                    </div>
                    <div>
                        <label><b>{'Chức năng: '}</b></label>
                    </div>
                    {objectOptions}
                </ModalBody>
                <ModalFooter>
                    <button
                        className="btn btn-success"
                        onClick={ this.submit }>
                        <i className="fa fa-save"></i> {' Lưu '}
                    </button>
                    <button
                        className="btn topica-btn-gray"
                        onClick={ this.props.toggleModal }>
                        <i className="fa fa-close"></i> {' Đóng '}
                    </button>
                </ModalFooter>
            </Modal>
        );
    }
}