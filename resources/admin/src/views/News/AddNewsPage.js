/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
// import axios                        from 'axios';
import BaseComponent                from '../BaseComponent';
import ImageUploader                from 'react-images-upload';
import Select                       from 'react-select'

export default class AddNewsPage extends BaseComponent
{
    constructor(props)
    {
        super(props);
        this.state = {
            news : {},
            listNewsCategory: [],
        };
        this.listData = [
            {
                link : "/admin/news-categories",
                state_key :"listNewsCategory"
            },
        ];
        if(this.props.match.params.id) {
            this.listData.push(
                {
                    link : "/admin/newses/" + this.props.match.params.id,
                    state_key :"news"
                }
            )
        };
        this.submit         = this.submit.bind(this);
        this.onChangeImage         = this.onChangeImage.bind(this);

    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            let result = await this.loadData(data.link);
            if(result) {
                this.state[data.state_key] = result;
            }
        }
        if(this.state.news.id) {
            this.state.avatarUrl = '/img/news-avatars/news-' + this.state.news.id + '.png';
        }
        this.reset();
    }

    async submit()
    {
        let {
            news,
        } = this.state;
        let submitStatus = true;
        if(news.id) {
            let result = await this.putData('/admin/newses/' + news.id, news);
            if (result != 0) {
                this.state.news = result;
                this.reset();
            } else {
                submitStatus = false;
            }
        } else {
            let result = await this.postData('/admin/newses', news);
            if (result != 0) {
                this.state.news = result;
                this.reset();
            } else {
                submitStatus = false;
            }
        }
        if(submitStatus) {
            Popup.alert('Cập nhật thông tin thành công!');
        }
    }

    onChangeImage(files) {
        console.log('onChangeImage');
        let picture = files[files.length - 1];
        var reader = new FileReader();
        let {news} = this.state;
        reader.onloadend = () => {
            news.avatar_data = reader.result;
            this.setState({ avatarUrl: URL.createObjectURL(picture) });
        }
        reader.readAsDataURL(picture);
    }

    render()
    {
        let {
            news,
            listNewsCategory,
            avatarUrl,
        } = this.state;

        let newsCategoryOptions = [];
        listNewsCategory.forEach(
            newsCate => {
                // newsCategoryOptions.push(
                //     <option value={newsCate.id} key ={newsCate.id}>{newsCate.name}</option>
                // );
                newsCategoryOptions.push({ label: newsCate.name, value: newsCate.id });
            }
        )

        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-header">{ 'Thêm mới tin tức' }</div>
                    <div className="card-body">
                        <div className="row form-group">
                            <div className="col-sm-3">
                                <label> { 'Tiêu đề tin tức' } </label>
                            </div>
                            <div className="col-sm-4">
                                <input
                                    className="form-control"
                                    autoFocus={ true }
                                    key={ news.id + this.state.reset }
                                    defaultValue={ news.name }
                                    onChange={
                                        e => {
                                            news.name = e.target.value
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-3">
                                <label> { 'Ảnh đại diện' } </label>
                            </div>
                            <div className="col-sm-4">
                                <ImageUploader
                                    withIcon={true}
                                    buttonText='Choose images'
                                    onChange={this.onChangeImage}
                                    imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                    maxFileSize={5242880}
                                />
                            </div>
                            <div className="col-sm-4">
                                <img
                                    src={ avatarUrl && avatarUrl.indexOf('news-avatars') > -1 ? (avatarUrl + '?' + Math.random()) : avatarUrl }
                                    className="img-news-avatar"/>
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-3"><label> {'Danh mục'}</label> </div>
                            <div className="col-sm-4">
                                <Select
                                    multi
                                    simpleValue
                                    removeSelected={true}
                                    closeOnSelect ={false}
                                    value={ news.categories }
                                    options={ newsCategoryOptions }
                                    placeholder="Chọn danh mục"
                                    onChange={
                                        value => {
                                            news.categories = value;
                                            this.setState({news: news});
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-3"><label> {'Nội dung tin tức'}</label> </div>
                            <div className="col-sm-9">
                                <textarea
                                    className="form-control"
                                    rows="10"
                                    key={ news.id + this.state.reset }
                                    defaultValue={ news.content }
                                    onChange={
                                        e => {
                                            news.content = e.target.value
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row form-group">
                            <div className="col-sm-3"><label> {'Từ khóa'}</label> </div>
                            <div className="col-sm-9">
                                <input
                                    className="form-control"
                                    key={ news.id + this.state.reset }
                                    defaultValue={ news.key_word }
                                    onChange={
                                        e => {
                                            news.key_word = e.target.value
                                        }
                                    }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-7">
                                <button
                                    className="btn btn-success float-right"
                                    onClick={ this.submit }
                                >
                                    <i className="fa fa-save"></i>{' Lưu '}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

