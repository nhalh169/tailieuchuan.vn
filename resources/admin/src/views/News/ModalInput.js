/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 20/06/2018
 * Modified at: 20/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


import React, { Component }     from 'react';
import Popup                    from 'react-popup';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    FormGroup,
    Label,
    Input,
}                               from 'reactstrap';
import BaseComponent            from '../BaseComponent';

export default class ModalInput extends BaseComponent
{
    constructor(props)
    {
        super(props);

        this.state = {
            newsCategory : this.props.existData ? {...this.props.existData}: {},
        };

        this.listData = [
            // {
            //     link : "/products",
            //     state_key :"listProduct"
            // },
        ];

        this.submitApi = '/admin/news-categories';

        this.validateData               = this.validateData.bind(this);
        this.submit                     = this.submit.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
    }

    validateData()
    {
        let { newsCategory } = this.state;
        if(!newsCategory.name || newsCategory.name.length == 0) {
            Popup.alert("Vui lòng điền tên nhóm thành viên!");
            return;
        }
        if(newsCategory.name.length > 200) {
            Popup.alert("Tên nhóm thành viên quá dài!");
            return;
        }
        this.submit();
    }

    async submit()
    {
        let { newsCategory } = this.state;
        let result = false;
        if(newsCategory.id) {
            result = await this.putData(this.submitApi + "/" + newsCategory.id, newsCategory);
        } else {
            result = await this.postData(this.submitApi, newsCategory);
        }
        if(result != false) {
            Popup.alert('Cập nhật thông tin thành công!');
            this.props.reloadListData();
            this.props.toggleModal();
        }
    }
    render()
    {
        let {
            newsCategory,
        } = this.state;
        return(
            <Modal
                className="modal-lg"
                isOpen = {this.props.showModal}
                toggle = {this.props.toggleModal}>
                <ModalHeader toggle = {this.props.toggleModal}>
                    {newsCategory.id ? ("Sửa danh mục tin: " + this.state.newsCategory.name) : "Tạo danh mục tin"}
                </ModalHeader>
                <ModalBody>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Tên danh mục'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ this.state.newsCategory.name }
                                onChange={ e => { this.state.newsCategory.name = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Thứ tự'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                defaultValue={ this.state.newsCategory.order }
                                onChange={ e => { this.state.newsCategory.order = e.target.value } }
                            />
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Mô tả'}</label> </div>
                        <div className="col-sm-9">
                            <textarea
                                className="form-control"
                                rows={'6'}
                                defaultValue={ this.state.newsCategory.description }
                                onChange={ e => { this.state.newsCategory.description = e.target.value } }
                            />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button
                        className="btn btn-success"
                        onClick={ this.submit }>
                        <i className="fa fa-save"></i> {' Lưu '}
                    </button>
                    <button
                        className="btn topica-btn-gray"
                        onClick={ this.props.toggleModal }>
                        <i className="fa fa-close"></i> {' Đóng '}
                    </button>
                </ModalFooter>
            </Modal>
        );
    }
}