/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import {NavLink}                    from 'react-router-dom';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class NewsPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listNews : [],
            listNewsCategory : [],
        }

        this.listData = [
            {
                link : "/admin/newses",
                state_key :"listNews"
            },
            {
                link : "/admin/news-categories",
                state_key :"listNewsCategory"
            },
        ];

        this.reloadListData             = this.reloadListData.bind(this);
        this.validateDeleteNews          = this.validateDeleteNews.bind(this);
        // this.deleteNews            = this.deleteNews.bind(this);
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listNews = await this.loadData('/admin/newses', true);
        this.reset();
    }

    validateDeleteNews(news)
    {
        if(news && news.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa danh mục tin',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa danh mục tin ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData("/admin/group-users/" + news.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        } else {
            Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
            return;
        }
    }

    render() {
        let {
            listNews,
            listNewsCategory,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Tin tức' }</label>
                        {/*<AddButton reloadListData={ this.reloadListData }/>*/}
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listNews }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listNews.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tin tức' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='categories'
                                dataSort
                                width={'300'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        let result = [];
                                        if(cell) {
                                            let categories = cell.split(',');
                                            categories.forEach(
                                                categoryId => {
                                                    let category = listNewsCategory.find(c => c.id == categoryId);
                                                    if(category) {
                                                        result.push(category.name);
                                                    }
                                                }
                                            )
                                        }
                                        return result.join(', ');
                                    }
                                }
                            >
                                { 'Danh mục' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <button
                                                    className="btn btn-sm btn-primary"
                                                    onClick={() => this.props.history.push("/admin/news/" + row.id + '/edit')}
                                                >
                                                    <i className="fa fa-edit"></i> {' Edit'}
                                                </button>
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteNews(row)}
                                                >
                                                    <i className="fa fa-trash"></i> {'Xóa'}
                                                </button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số lượt xem' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='creator'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Người tạo' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='approver'
                                dataSort
                                width="150"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Người duyệt' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='approver'
                                dataSort
                                width="150"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Nổi bật trang chủ' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='approver'
                                dataSort
                                width="150"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Nổi bật danh mục' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

