/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import EditButton                    from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class NewsCategoryPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listNewsCategory : [],
            listRole : [],
            listProduct : [],
        }

        this.listData = [
            {
                link : "/admin/news-categories",
                state_key :"listNewsCategory"
            },
        ];
        
        this.submitApi = '/admin/news-categories';

        this.reloadListData             = this.reloadListData.bind(this);
        this.validateDeleteNewsCategory          = this.validateDeleteNewsCategory.bind(this);
        // this.deleteNewsCategory            = this.deleteNewsCategory.bind(this);
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listNewsCategory = await this.loadData(this.submitApi, true);
        this.reset();
    }

    validateDeleteNewsCategory(newsCategory)
    {
        // if(newsCategory && newsCategory.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa danh mục tin',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa danh mục tin ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData(this.submitApi + '/' + newsCategory.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        // } else {
        //     Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
        //     return;
        // }
    }

    render() {
        let {
            listNewsCategory,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Danh mục tin' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listNewsCategory }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listNewsCategory.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tên Danh Mục' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='description'
                                dataAlign='right'
                                dataSort
                                width={'300'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Mô tả' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_news'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số tin tức' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số lượt xem' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='created_at'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Vị trí' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <EditButton
                                                    existData={row}
                                                    reloadListData={ this.reloadListData }
                                                />
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteNewsCategory(row)}
                                                > <i className="fa fa-trash"></i> {'Xóa'}</button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

