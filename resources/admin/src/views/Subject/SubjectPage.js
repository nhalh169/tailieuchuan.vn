/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import EditButton                    from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class SubjectPage extends BaseComponent {

    constructor(props){

        super(props);
        this.state = {
            listSubject : [],
            listCategory : [],
        }
        this.submitApi = '/admin/subjects';

        this.listData = [
            {
                link : this.submitApi,
                state_key :"listSubject"
            },
            {
                link : '/admin/categories',
                state_key :"listCategory"
            },
        ];

        this.reloadListData                 = this.reloadListData.bind(this);
        this.validateDeleteSubject          = this.validateDeleteSubject.bind(this);
        this.getMapOfCategory               = this.getMapOfCategory.bind(this);
        // this.deleteSubject            = this.deleteSubject.bind(this);
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listSubject = await this.loadData(this.submitApi, true);
        this.reset();
    }

    validateDeleteSubject(subject)
    {
        // if(subject && subject.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa chủ đề',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa chủ đề ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData(this.submitApi + '/' + subject.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        // } else {
        //     Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
        //     return;
        // }
    }

    getMapOfCategory(category_id)
    {
        let { listCategory } = this.state;
        let cate = listCategory.find(c => c.id == category_id);
        if(cate) {
            let result = cate.name;
            let parentCate = listCategory.find(c => c.id == cate.parent_id);
            if(parentCate) {
                result = this.getMapOfCategory(parentCate.id) + ' -> ' + result;
            }
            return result;
        } else {
            console.log("Can not find cate: " + category_id);
        }
    }

    render() {
        let {
            listSubject,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Chủ đề' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listSubject }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listSubject.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tên chủ đề' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='category_id'
                                dataSort
                                dataTitle
                                width={'250'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    cell => {
                                        return this.getMapOfCategory(cell);
                                    }
                                }
                            >
                                { 'Danh mục' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='description'
                                dataAlign='right'
                                dataSort
                                width={'200'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Mô tả' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_news'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số tin tức' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Số lượt xem' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='created_at'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Ngày tạo' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <EditButton
                                                    existData={row}
                                                    reloadListData={ this.reloadListData }
                                                />
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteSubject(row)}
                                                > <i className="fa fa-trash"></i> {'Xóa'}</button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

