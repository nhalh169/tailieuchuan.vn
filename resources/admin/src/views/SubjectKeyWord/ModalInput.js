/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 20/06/2018
 * Modified at: 20/06/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/


import React, { Component }     from 'react';
import Popup                    from 'react-popup';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Col,
    FormGroup,
    Label,
    Input,
}                               from 'reactstrap';
import BaseComponent            from '../BaseComponent';

export default class ModalInput extends BaseComponent
{
    constructor(props)
    {
        super(props);

        this.state = {
            subjectKeyWord : this.props.existData ? { ...this.props.existData }: {},
            listCategory : [],
            listSubject : [],
        };

        this.listData = [
            {
                link: '/admin/categories',
                state_key: 'listCategory'
            },
            {
                link: '/admin/subjects',
                state_key: 'listSubject'
            },
        ];

        this.submitApi = '/admin/subject-key-words';

        this.validateAndSubmit          = this.validateAndSubmit.bind(this);
        this.submit                     = this.submit.bind(this);
        this.renderSelectCategory       = this.renderSelectCategory.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i ++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
    }

    validateAndSubmit()
    {
        let { listCategory, subjectKeyWord } = this.state;
        let childCate = listCategory.filter(c => c.parent_id == subjectKeyWord.category_id);
        if(childCate.length > 0) {
            Popup.alert("Danh mục này chứa danh mục con. Vui lòng chọn danh mục khác !");
            return;

        }
        if(!subjectKeyWord.name || subjectKeyWord.name.length == 0) {
            Popup.alert("Vui lòng điền tên chủ đề!");
            return;
        }
        if(subjectKeyWord.name.length > 128) {
            Popup.alert("Tên chủ đề quá dài!");
            return;
        }
        this.submit();
    }

    async submit()
    {
        let { subjectKeyWord } = this.state;
        let result = false;
        if(subjectKeyWord.id) {
            result = await this.putData(this.submitApi + "/" + subjectKeyWord.id, subjectKeyWord);
        } else {
            result = await this.postData(this.submitApi, subjectKeyWord);
        }
        if(result != false) {
            Popup.alert('Cập nhật thông tin thành công!');
            this.props.reloadListData();
            this.props.toggleModal();
        }
    }

    renderSelectCategory(categoryId)
    {
        let { listCategory, subjectKeyWord } = this.state;
        let listSelectCategory = [];
        let category = listCategory.find(c => c.id == categoryId);
        if(!category || categoryId == 0) {
            let firstCategories = listCategory.filter(c => c.parent_id == 0);
            let categoryOptions = [];
            categoryOptions.push(
                <option value={0} key={0}>{'Chọn danh mục'}</option>
            )
            firstCategories.forEach(
                cate => {
                    categoryOptions.push(
                        <option value={cate.id} key={cate.id}>{cate.name}</option>
                    )
                }
            )
            return(
                <select
                    key={'select-category-' + categoryId}
                    className="form-control mb-2"
                    defaultValue={ subjectKeyWord.category_id }
                    onChange={ e => { subjectKeyWord.category_id = e.target.value; this.reset(); } }
                >
                    {categoryOptions}
                </select>
            )

        }
        let parentCategory = listCategory.find(c => c.id == category.parent_id);
        // render parent category
        if(parentCategory && category.parent_id != subjectKeyWord.category_id) {
            listSelectCategory.push(this.renderSelectCategory(parentCategory.id));
        }

        let brotherCategory = listCategory.filter(c => c.parent_id == category.parent_id);
        let brotherCategoryOptions = [];
        brotherCategoryOptions.push(
            <option value={category.parent_id} key={0}>{'Chọn danh mục'}</option>
        )
        brotherCategory.forEach(
            cate => {
                brotherCategoryOptions.push(
                    <option value={cate.id} key={cate.id}>{cate.name}</option>
                )
            }
        )
        listSelectCategory.push(
            <select
                key={ 'select-category-' + categoryId }
                className="form-control mb-2"
                defaultValue={ category.parent_id == subjectKeyWord.category_id ? '' : categoryId }
                onChange={ e => { subjectKeyWord.category_id = e.target.value; this.reset(); } }
            >
                {brotherCategoryOptions}
            </select>
        )

        let childCategory = listCategory.filter(c => c.parent_id == category.id);
        // render child category
        if(childCategory.length > 0 && category.id == subjectKeyWord.category_id) {
            listSelectCategory.push(this.renderSelectCategory(childCategory[0].id));
        }
        return listSelectCategory;
        return(
            <div key="list-select-category" className="list-select-category">
                {listSelectCategory}
            </div>
        )
    }

    render()
    {
        let {
            subjectKeyWord,
            listSubject,
        } = this.state;
        let subjectOptions = [];
        subjectOptions.push(<option key="default" value=''>{ 'Chọn chủ đề' }</option>)
        listSubject.filter(s => s.category_id == subjectKeyWord.category_id).forEach(
            subject => {
                subjectOptions.push(
                    <option key={ subject.id } value={ subject.id }>{ subject.name }</option>
                )
            }
        )
        return(
            <Modal
                className="modal-lg"
                isOpen = {this.props.showModal}
                toggle = {this.props.toggleModal}>
                <ModalHeader toggle = {this.props.toggleModal}>
                    {subjectKeyWord.id ? ("Sửa từ khóa: " + this.state.subjectKeyWord.name) : "Tạo từ khóa"}
                </ModalHeader>
                <ModalBody>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Danh mục'}</label> </div>
                        <div className="col-sm-4">
                            { this.renderSelectCategory(subjectKeyWord.category_id) }
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Chủ đề'}</label> </div>
                        <div className="col-sm-4">
                            <select
                                className="form-control"
                                defaultValue={ subjectKeyWord.subject_id }
                                onChange={ e => { subjectKeyWord.subject_id = e.target.value } }
                            >
                                { subjectOptions }
                            </select>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-3"><label>{'Tên từ khóa'}</label> </div>
                        <div className="col-sm-4">
                            <input
                                className="form-control"
                                autoFocus={ true }
                                defaultValue={ subjectKeyWord.name }
                                onChange={ e => { this.state.subjectKeyWord.name = e.target.value } }
                            />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button
                        className="btn btn-success"
                        onClick={ this.validateAndSubmit }>
                        <i className="fa fa-save"></i> {' Lưu '}
                    </button>
                    <button
                        className="btn topica-btn-gray"
                        onClick={ this.props.toggleModal }>
                        <i className="fa fa-close"></i> {' Đóng '}
                    </button>
                </ModalFooter>
            </Modal>
        );
    }
}