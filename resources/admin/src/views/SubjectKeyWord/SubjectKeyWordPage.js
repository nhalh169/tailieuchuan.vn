/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
import AddButton                    from './AddButton';
import EditButton                    from './EditButton';
import BaseComponent                from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class SubjectKeyWordPage extends BaseComponent 
{

    constructor(props)
    {
        super(props);
        this.state = {
            listSubjectKeyWord : [],
            listCategory : [],
            listSubject : [],
        }
        this.submitApi = '/admin/subject-key-words';

        this.listData = [
            {
                link : this.submitApi,
                state_key :"listSubjectKeyWord"
            },
            {
                link : '/admin/categories',
                state_key :"listCategory"
            },
            {
                link : '/admin/subjects',
                state_key :"listSubject"
            },
        ];

        this.reloadListData                 = this.reloadListData.bind(this);
        this.validateDeleteSubjectKeyWord          = this.validateDeleteSubjectKeyWord.bind(this);
        this.getMapOfCategory               = this.getMapOfCategory.bind(this);
        // this.deleteSubjectKeyWord            = this.deleteSubjectKeyWord.bind(this);
    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async reloadListData()
    {
        this.state.listSubjectKeyWord = await this.loadData(this.submitApi, true);
        this.reset();
    }

    validateDeleteSubjectKeyWord(subjectKeyWord)
    {
        // if(subjectKeyWord && subjectKeyWord.number_user == 0) {
            Popup.create({
                className: '',
                title:'Xác nhận xóa từ khóa',
                content:
                    <div className="">
                        {'Bạn có chắc chắn xóa từ khóa ? Bạn không thể hoàn lại sau khi xóa.'}
                    </div>,
                buttons: {
                    right: [
                        {
                            text: 'Xóa',
                            className: 'danger',
                            action: async () => {
                                Popup.close();
                                await this.deleteData(this.submitApi + '/' + subjectKeyWord.id);
                                await this.reloadListData();
                            },
                        },
                        {
                            text: 'Hủy bỏ',
                            className: '',
                            action: Popup.close,
                        },
                    ]
                },
                closeOnOutsideClick: false,
            })
        // } else {
        //     Popup.alert('Nhóm vẫn còn người dùng! Vui lòng kiểm tra lại');
        //     return;
        // }
    }

    getMapOfCategory(category_id)
    {
        let { listCategory } = this.state;
        let cate = listCategory.find(c => c.id == category_id);
        if(cate) {
            let result = cate.name;
            let parentCate = listCategory.find(c => c.id == cate.parent_id);
            if(parentCate) {
                result = this.getMapOfCategory(parentCate.id) + ' -> ' + result;
            }
            return result;
        } else {
            console.log("Can not find cate: " + category_id);
        }
    }

    render() {
        let {
            listSubjectKeyWord,
            listSubject,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label>{ 'Từ khóa' }</label>
                        <AddButton reloadListData={ this.reloadListData }/>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={ listSubjectKeyWord }
                            pagination
                            search={ true }
                            options={ this.getDefaultOptions(listSubjectKeyWord.length) }
                        >
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'ID' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='name'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tên từ khóa' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='category_id'
                                dataSort
                                dataTitle
                                width={'250'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    cell => {
                                        return this.getMapOfCategory(cell);
                                    }
                                }
                            >
                                { 'Danh mục' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='subject_id'
                                dataSort
                                dataTitle
                                width={'150'}
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    cell => {
                                        let subject = listSubject.find(s => s.id == cell);
                                        return subject ? subject.name : '';
                                    }
                                }
                            >
                                { 'Chủ đề' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_news'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Tài liệu' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='count_view'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Lượt xem' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='created_at'
                                dataSort
                                width="100"
                                filter={ { type: 'TextFilter'} }
                            >
                                { 'Ngày tạo' }
                            </TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='edit'
                                dataSort
                                width="200"
                                filter={ { type: 'TextFilter'} }
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <div>
                                                <EditButton
                                                    existData={row}
                                                    reloadListData={ this.reloadListData }
                                                />
                                                <button
                                                    className="ml-2 btn btn-danger  btn-sm"
                                                    onClick={ () => this.validateDeleteSubjectKeyWord(row)}
                                                > <i className="fa fa-trash"></i> {'Xóa'}</button>
                                            </div>
                                        )
                                    }
                                }
                            >
                                { 'Thao tác' }
                            </TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

