/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
// import AddUserButton             from './AddUserButton';
// import EditUserButton            from './EditUserButton';
// import DeleteUserButton          from './DeleteUserButton';
import axios                        from 'axios';
import BaseComponent                 from '../BaseComponent';

export default class EditUserPage extends BaseComponent
{
    constructor(props)
    {
        super(props);
        this.state = {
            user : {},
            listGroupUser : [],
        }
        this.user_id = this.props.match.params.id ? this.props.match.params.id : 0;

        this.listData = [
            {
                link : "/users/" + this.user_id,
                state_key :"user"
            },
            {
                link : "/admin/group-users",
                state_key :"listGroupUser"
            },
        ];

        this.submit             = this.submit.bind(this);

    }

    async componentWillMount()
    {
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    async submit()
    {
        let {user} = this.state;
        if (!user.full_name) {
            Popup.alert('Bạn chưa nhập tên đầy đủ');
            return
        }
        let result = await this.putData('/users/' + user.id, user);
        if (result != 0) {
            this.state.user = result;
            this.reset();
            Popup.alert('Cập nhật thông tin thành công!');
        }
    }

    render() {
        let {
            user,
            listGroupUser
        } = this.state;

        let birthdayOptions = [];
        for (let i = 1900; i < new Date().getFullYear(); i++) {
            birthdayOptions.push(
                <option value={i} key={i}>{i}</option>
            )
        }
        let titleOptions = [];
        titleOptions.push(
            <option value={''} key={'default'}>{'Lựa chọn chức danh'}</option>
        )
        let listTitle = [
            {
                id: 1,
                name: 'Giáo Viên Toán'
            },
            {
                id: 2,
                name: 'Giáo Viên Lý'
            },
        ];
        listTitle.forEach(
            title => {
                titleOptions.push(
                    <option value={title.id} key={title.id}>{title.name}</option>
                )
            }
        )

        let classOptions = [];
        for (let i = 1; i < 13; i++) {
            classOptions.push(
                <option value={i} key={i}>{i}</option>
            )
        }
        let groupOptions = [];
        listGroupUser.forEach(
            group => {
                groupOptions.push(
                    <option value={group.id} key={group.id}>{group.name}</option>
                )
            }
        )
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="card">
                            <div className="card-header">
                                {'Thông tin chung'}
                            </div>
                            <div className="card-body">
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Email'}</div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            disabled
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={user.email}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Tên đầy đủ'}</div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.full_name = e.target.value}}
                                            defaultValue={user.full_name}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Số điện thoại'}</div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.phone = e.target.value}}
                                            defaultValue={user.phone}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Năm sinh'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.birth_day = e.target.value}}
                                            defaultValue={user.birth_day}>
                                            {birthdayOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Bạn là'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.title_id = e.target.value}}
                                            defaultValue={user.title_id}>
                                            {titleOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Lớp'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.class = e.target.value}}
                                            defaultValue={user.class}>
                                            {classOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Thành phố ---'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.city = e.target.value}}
                                            defaultValue={user.city}>
                                            {classOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Quận/ Huyện---'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={user.class}>
                                            {classOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4">{'Trường học ---'}</div>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={user.class}>
                                            {classOptions}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">{'Thông tin thanh toán'}</div>
                            <div className="card-body">
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4"><b>{'Số tài khoản'}</b></div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            // disabled
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.pay_number = e.target.value}}
                                            defaultValue={user.pay_number}
                                        />
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4"><b>{'Chủ tài khoản'}</b></div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            // disabled
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.pay_name = e.target.value}}
                                            defaultValue={user.pay_name}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4"><b>{'Ngân hàng---'}</b></div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            disabled
                                            key={user.id + '_' + this.state.reset}
                                            // onChange={e => {user.bank_id = e.target.value}}
                                            defaultValue={'Vietcombank'}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-4"><b>{'Chi nhánh---'}</b></div>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            disabled
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={'Hai Bà Trưng'}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="card">
                            <div className="card-header">{'Chức năng thành viên'}</div>
                            <div className="card-body">
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Nhóm thành viên'}</div>
                                    <div className="col-sm-5">
                                        <select
                                            className="form-control"
                                            type="text"
                                            key={groupOptions.length + '_' + this.state.reset}
                                            onChange={e => {user.group_id = e.target.value}}
                                            defaultValue={user.group_id}>
                                            {groupOptions}
                                        </select>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Cộng tiền số dư chính ---'}</div>
                                    <div className="col-sm-5">
                                        <input
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={'0'}/>
                                    </div>
                                    <label htmlFor="">{'100,000'}</label>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Cộng tiền số dư khuyến mãi ---'}</div>
                                    <div className="col-sm-5">
                                        <input
                                            className="form-control"
                                            type="text"
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={'0'}/>
                                    </div>
                                    <label htmlFor="">{'20,000'}</label>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Chiết khấu'}</div>
                                    <div className="col-sm-2">
                                        <input
                                            className="form-control"
                                            type="number"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.discount = e.target.value}}
                                            defaultValue={user.discount}/>
                                    </div>
                                    <label htmlFor="">{'%'}</label>
                                    <div className="col-sm-4">
                                        <div className="position-relative form-check form-check-inline">
                                            <input
                                                className="mr-2 ml-2 form-check-input"
                                                type="checkbox"
                                                id="fixed-discount"
                                                key={user.id + '_' + this.state.reset}
                                                onChange={e => {user.fixed_discount = e.target.checked;}}
                                                defaultChecked={user.fixed_discount == 1}/>
                                            <label
                                                className="form-check-label"
                                                htmlFor="fixed-discount">
                                                {'Không thay đổi chiết khấu'}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Ưu tiên duyệt tài liệu'}</div>
                                    <div className="col-sm-9">
                                        <input
                                            className="mr-2"
                                            type="checkbox"
                                            id="duyet-tai-lieu"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.prioritize_member = e.target.checked;}}
                                            defaultChecked={user.prioritize_member == 1 ? 1 : 0}/>
                                        <label htmlFor="duyet-tai-lieu">{' Ưu tiên'}</label>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Block tài khoản'}</div>
                                    <div className="col-sm-1">
                                        <div className="form-check form-check-inline">
                                            <input
                                                className="form-check-input"
                                                type="checkbox"
                                                id="block-status"
                                                key={user.id + '_' + this.state.reset}
                                                onChange={e => {user.block_status = e.target.checked;}}
                                                defaultChecked={user.block_status == 1 ? 1 : 0}
                                            />
                                            <label
                                                className="form-check-label"
                                                htmlFor="block-status">
                                                {'Block'}
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <input
                                            className="form-control"
                                            type="text"
                                            placeholder="Lý do block"
                                            key={user.id + '_' + this.state.reset}
                                            onChange={e => {user.block_reason = e.target.value;}}
                                            defaultValue={user.block_reason}/>
                                    </div>
                                </div>
                                <div className="position-relative row form-group">
                                    <div className="col-sm-3">{'Khóa sử dụng tiền'}</div>
                                    <div className="col-sm-1">
                                        <div className="form-check form-check-inline">
                                            <input
                                                className="form-check-input"
                                                type="checkbox"
                                                id="lock-use-money"
                                                key={user.id + '_' + this.state.reset}
                                                defaultValue={'0'}/>
                                            <label
                                                className="form-check-label"
                                                htmlFor="lock-use-money">
                                                {'Khóa'}
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <input
                                            className="form-control"
                                            type="text"
                                            placeholder="09/10/2018"
                                            key={user.id + '_' + this.state.reset}
                                            defaultValue={user.block_message}/>
                                    </div>
                                    <label>{'Chọn ngày tài khoản bị khóa'}</label>
                                </div>
                                <div >
                                    <b>{'Giới hạn danh mục được tải: '}</b>
                                </div>
                                <br/>
                                <br/>
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="position-relative form-check form-check-inline">
                                            <label
                                                htmlFor="popup-notify"
                                                className="form-check-label  mr-2">
                                                {'Thông báo Popup'}
                                                </label>
                                            <input
                                                id="popup-notify"
                                                type="checkbox"/>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <textarea
                                            className="form-control"
                                            rows="10">
                                        </textarea>
                                    </div>
                                </div>
                                <div className="mt-3">
                                    <button
                                        className="btn btn-success"
                                        onClick={this.submit}
                                    >
                                        {'Lưu'}
                                        </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

