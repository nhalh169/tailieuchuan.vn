/**
 *  ${FILE_NAME}
 *   Desciption of file
 *
 * Author     : NhaLH
 * Created at : 25/05/2018
 * Modified at: 25/05/2018
 *
 *
 * Copyright (c)-2017 TOPICA EDTECH GROUP (www.topica.asia)
 *****************************************************************************/

import React, { Component }         from 'react';
import Popup                        from 'react-popup';
// import AddUserButton             from './AddUserButton';
// import EditUserButton            from './EditUserButton';
// import DeleteUserButton          from './DeleteUserButton';
import BaseComponent                 from '../BaseComponent';
import {
    BootstrapTable,
    TableHeaderColumn
}                                from 'react-bootstrap-table';

export default class UserPage extends BaseComponent {

    constructor(props){
        super(props);

        this.state = {
            listUser : [],
            listGroupUser : [],
        }
        this.listData = [
            {
                link : "/admin/users",
                state_key :"listUser"
            },
            {
                link : "/admin/group-users",
                state_key :"listGroupUser"
            },
        ];
    }

    async componentWillMount(){
        for(let i = 0; i < this.listData.length; i++) {
            let data = this.listData[i];
            this.state[data.state_key] = await this.loadData(data.link);
        }
        this.reset();
    }

    render() {
        let {
            listUser,
        } = this.state;

        return (
            <div className="animated fadeIn custom-over-x">
                <div className="card">
                    <div className="card-header">
                        <label className="mr-2">{ 'Thành viên' }</label>
                    </div>
                    <div className="card-body">
                        <BootstrapTable
                            data={listUser}
                            pagination
                            search={true}
                            options={this.getDefaultOptions(listUser.length)}>
                            <TableHeaderColumn
                                dataField='id'
                                width={'70'}
                                dataSort
                                isKey
                                filter={ { type: 'TextFilter'}}>
                                ID</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='created_at'
                                width={'150'}
                                dataSort
                                filter={ { type: 'TextFilter'}}>
                                Ngày tạo</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='full_name'
                                width={'150'}
                                filter={ { type: 'TextFilter'}}
                                dataFormat={
                                    (cell, row) => {
                                        return(
                                            <a
                                                href={'/admin/user/' + row.id + '/edit'}
                                                title={cell}
                                            >
                                                {cell}
                                            </a>
                                        )
                                    }
                                }
                            >
                                Tên đầy đủ</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='email'
                                columnTitle
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Email</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='phone'
                                dataAlign='right'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Số điện thoại</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='title_id'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Chức danh</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='birthday'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Năm sinh</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='class'
                                dataSort
                                width={'70'}
                                filter={ { type: 'TextFilter'}}>
                                Lớp</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='school_id'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Trường</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='city'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Thành Phố</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='city'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Quận</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='group_id'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Nhóm</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='total_credit'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Tổng nạp</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='main_credit'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Số dư chính</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='gift_credit'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Số dư KM</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='current_revenue'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Thu nhập hiện tại</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='total_revenue'
                                dataSort
                                width={'150'}
                                filter={ { type: 'TextFilter'}}>
                                Tổng thu nhập</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='discount'
                                dataSort
                                width={'120'}
                                filter={ { type: 'TextFilter'}}>
                                Chiết khấu</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='discount'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Đã tải</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='discount'
                                dataSort
                                width={'100'}
                                filter={ { type: 'TextFilter'}}>
                                Đã đăng</TableHeaderColumn>
                            <TableHeaderColumn
                                dataField='id'
                                width={'100'}
                                dataFormat={ (cell, row) => {
                                    return (
                                            <div>
                                                <div>
                                                    <i
                                                        className="fa fa-trash mr-1"
                                                        title="Xóa người dùng"
                                                    ></i>
                                                    <i
                                                        className="fa fa-edit mr-1"
                                                        title="Sửa thông tin người dùng"
                                                    ></i>
                                                    <i
                                                        className="fa fa-lock mr-1"
                                                        title="Khoá người dùng"
                                                    ></i>
                                                </div>
                                                <div>
                                                    <i
                                                        className="fa fa-sticky-note mr-1"
                                                        title="Sửa ghi chú 1"
                                                    ></i>
                                                    <i
                                                        className="fa fa-sticky-note mr-1"
                                                        title="Sửa ghi chú 2"
                                                    ></i>
                                                    <i
                                                        className="fa fa-history mr-1"
                                                        title="Xem lịch sử hoạt động"
                                                    ></i>
                                                </div>
                                            </div>
                                    )
                                }}>
                                Thao tác</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        );
    }
}

