<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin router ----- All router have prefix is admin
Route::get('/', function () {
    return view('admin');
});

//User router
Route::get('/users', 'User\UserController@getAllUser' );

//Group User router
Route::get('/group-users', 'GroupUser\GroupUserController@getAllGroupUser' );
Route::get('/group-users/all-data', 'GroupUser\GroupUserController@getAllData' );
Route::post('/group-users', 'GroupUser\GroupUserController@addGroupUser' );
Route::put('/group-users/{id}', 'GroupUser\GroupUserController@editGroupUser' );
Route::delete('/group-users/{id}', 'GroupUser\GroupUserController@deleteGroupUser' );

//admin config router
Route::get('/admin-configs/get-config-by-name', 'AdminConfig\AdminConfigController@getConfigByName' );
Route::post('/admin-configs', 'AdminConfig\AdminConfigController@addAdminConfig' );
Route::put('/admin-configs/{id}', 'AdminConfig\AdminConfigController@editAdminConfig' );

//Category router
Route::get('/categories', 'Category\CategoryController@getAllCategory' );
Route::post('/categories', 'Category\CategoryController@addCategory' );
Route::put('/categories/{id}', 'Category\CategoryController@editCategory' );
Route::delete('/categories/{id}', 'Category\CategoryController@deleteCategory' );

//News Category router
Route::get('/news-categories', 'NewsCategory\NewsCategoryController@getAllNewsCategory' );
Route::get('/news-categories/all-data', 'NewsCategory\NewsCategoryController@getAllData' );
Route::post('/news-categories', 'NewsCategory\NewsCategoryController@addNewsCategory' );
Route::put('/news-categories/{id}', 'NewsCategory\NewsCategoryController@editNewsCategory' );
Route::delete('/news-categories/{id}', 'NewsCategory\NewsCategoryController@deleteNewsCategory' );

//News router
Route::get('/newses', 'News\NewsController@getAllNews' );
Route::get('/newses/{id}', 'News\NewsController@getDetailNews' );
Route::post('/newses', 'News\NewsController@addNews' );
Route::put('/newses/{id}', 'News\NewsController@editNews' );
Route::delete('/newses/{id}', 'News\NewsController@deleteNews' );

//Subject router
Route::get('/subjects', 'Subject\SubjectController@getAllSubject' );
Route::get('/subjects/{id}', 'Subject\SubjectController@getDetailSubject' );
Route::post('/subjects', 'Subject\SubjectController@addSubject' );
Route::put('/subjects/{id}', 'Subject\SubjectController@editSubject' );
Route::delete('/subjects/{id}', 'Subject\SubjectController@deleteSubject' );

//Subject key word router
Route::get('/subject-key-words', 'SubjectKeyWord\SubjectKeyWordController@getAllSubjectKeyWord' );
Route::get('/subject-key-words/{id}', 'SubjectKeyWord\SubjectKeyWordController@getDetailSubjectKeyWord' );
Route::post('/subject-key-words', 'SubjectKeyWord\SubjectKeyWordController@addSubjectKeyWord' );
Route::put('/subject-key-words/{id}', 'SubjectKeyWord\SubjectKeyWordController@editSubjectKeyWord' );
Route::delete('/subject-key-words/{id}', 'SubjectKeyWord\SubjectKeyWordController@deleteSubjectKeyWord' );

//Subject router
Route::get('/documents', 'Document\DocumentController@getListApproved' );
Route::get('/documents-unapprover', 'Document\DocumentController@getListUnapproved' );
Route::get('/documents/{id}', 'Document\DocumentController@getDetailDocument' );
Route::post('/documents', 'Document\DocumentController@addDocument' );
Route::put('/documents/{id}', 'Document\DocumentController@editDocument' );
Route::delete('/documents/{id}', 'Document\DocumentController@deleteDocument' );


//Default router for admin
Route::get('/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}', function () {
    return view('admin');
});