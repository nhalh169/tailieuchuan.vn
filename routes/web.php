<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if( !defined("REACT_DEFAULT_ROUTER" )){
    define("REACT_DEFAULT_ROUTER",  'Home\HomeController@index' );
}

Route::get('/', function () {
    return view('home');
});

//User Router
Route::get('/users/{id}', 'User\UserController@getDetailUser' );
Route::post('/users', 'User\UserController@createUser' );
Route::put('/users/{id}', 'User\UserController@editUser' );
Route::delete('/users/{id}', 'User\UserController@deleteUser' );

// default router
Route::any('/{default?}/{a?}/{s?}/{d?}', REACT_DEFAULT_ROUTER);